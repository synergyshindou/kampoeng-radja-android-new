package cn.desert.newpos.payui.master;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputListener;
import com.android.desert.keyboard.InputManager;
import com.android.newpos.pay.R;
import com.newpos.libpay.Logger;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.PaySdkException;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.printer.PrintRes;
import com.newpos.libpay.device.user.OnUserResultListener;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.presenter.TransUIImpl;
import com.newpos.libpay.presenter.TransView;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.trans.manager.DparaTrans;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.PAYUtils;

import java.util.Locale;

import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.base.PayApplication;
/**
 * Created by zhouqiang on 2017/7/3.
 */

public class MasterControl extends AppCompatActivity implements TransView, View.OnClickListener{

    WebView wvInsert ;
    WebView wvSwipe ;
    WebView wvPat ;
    Button btnConfirm ;
    Button btnCancel ;
    EditText editCardNO ;
    EditText transInfo ;

    OnUserResultListener listener ;

    String inputContent ;

    public static String TRANS_KEY = "TRANS_KEY" ;

    private String type ;

    /**
     * add by vincent 2017.12.4
     * for app choose
     * */
    private int[] appId;
    private boolean isAppChoose = false;
    private int appNo = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PayApplication.getInstance().addActivity(this);
        DparaTrans.loadAIDCAPK2EMVKernel();
        type = getIntent().getStringExtra(TRANS_KEY);
        startTrans(ch2en(type));
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnCancel)){
            if (isAppChoose){
                ((TransUIImpl.IOnUserResultListener)listener).setOtherInfo(-1);//add by vincent
            }
            listener.cancel();
        }else if(view.equals(btnConfirm)){
            if (isAppChoose){
                ((TransUIImpl.IOnUserResultListener)listener).setOtherInfo(appNo);//add by vincent
            }
            listener.confirm(InputManager.Style.COMMONINPUT);
        }
        else {//add by vincent . For app choose
            switch (view.getId()){
                case R.id.appUnionId:
                    appNo=appId[0];
                    break;
                case R.id.appVisaId:
                    appNo = appId[1];
                    break;
                case R.id.appMSKAId:
                    appNo = appId[2];
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void showCardView(int timeout, int mode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.trans_show_card);
                loadWebGif();
            }
        });
    }

    @Override
    public void showQRCView(int timeout, InputManager.Style mode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.trans_show_qrc);
            }
        });
    }

    @Override
    public void showCardNo(int timeout, final String pan, OnUserResultListener l) {
        this.listener = l ;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.trans_show_cardno);
                showConfirmCardNO(PAYUtils.getSecurityNum(pan , 6 , 4));
            }
        });
    }

    @Override
    public void showInputView(int timeout, final InputManager.Mode mode, OnUserResultListener l) {
        this.listener = l ;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputManager inputManager = new InputManager(MasterControl.this) ;
                inputManager.setListener(new InputListener() {
                    @Override
                    public void callback(InputInfo inputInfo) {
                        if(inputInfo.isResultFlag()){
                            inputContent = inputInfo.getResult() ;
                            listener.confirm(inputInfo.getNextStyle());
                        }else {
                            listener.cancel();
                        }
                    }
                });
                if(Locale.getDefault().getLanguage().equals("zh")){
                    inputManager.setLang(InputManager.Lang.CH);
                }else {
                    inputManager.setLang(InputManager.Lang.EN);
                }

                if(mode == InputManager.Mode.AMOUNT){
                    inputManager.setTitle(R.string.please_input_amount);
                }if(mode == InputManager.Mode.PASSWORD){
                    inputManager.setTitle(R.string.please_input_master_pass);
                }if(mode == InputManager.Mode.VOUCHER){
                    inputManager.setTitle(R.string.please_input_trace_no);
                }if(mode == InputManager.Mode.AUTHCODE){
                    inputManager.setTitle(R.string.please_input_auth_code);
                }if(mode == InputManager.Mode.DATETIME){
                    inputManager.setTitle(R.string.please_input_data_time);
                }if(mode == InputManager.Mode.REFERENCE){
                    inputManager.setTitle(R.string.please_input_reference);
                }

                inputManager.addEdit(mode);

                if(mode == InputManager.Mode.PASSWORD){
                    inputManager.addKeyboard(true);
                }else {
                    inputManager.addKeyboard(false);
                }

                if(mode == InputManager.Mode.AMOUNT && ch2en(type).equals(Trans.Type.SALE)){
                    inputManager.addStyles();
                }

                setContentView(inputManager.getView());
            }
        });
    }

    @Override
    public String getInput(InputManager.Mode type) {
        return inputContent ;
    }

    @Override
    public void showTransInfoView(int timeout, final TransLogData data, OnUserResultListener l) {
        this.listener = l ;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.trans_show_transinfo);
                showOrignalTransInfo(data);
            }
        });
    }

    @Override
    public void showCardAppListView(int timeout, final String[] apps, TransUIImpl.IOnUserResultListener l) {
        this.listener = l ;
        isAppChoose = true;
        Logger.debug("call back showCardAppListView>>>>>>>>>>");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.applistchooseixml);
                showAppListInfo(apps);
            }
        });
    }

    @Override
    public void showMultiLangView(int timeout, String[] langs, OnUserResultListener l) {
        this.listener = l ;
    }

    @Override
    public void showSuccess(int timeout, final String info) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //UIUtils.toast(MasterControl.this , info);
                UIUtils.startResult(MasterControl.this , true , info);
            }
        });
    }

    @Override
    public void showError(int timeout, final String err) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //UIUtils.toast(MasterControl.this , err);
                UIUtils.startResult(MasterControl.this , false , err);
            }
        });
    }

    @Override
    public void showMsgInfo(int timeout, final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.trans_handling);
                showHanding(status);
            }
        });
    }

    private void startTrans(String type){
        try {
            PaySdk.getInstance().startTrans(type , this);
        } catch (PaySdkException e) {
            e.printStackTrace();
        }
    }

    public static String ch2en(String ch){
        String[] chs = PrintRes.TRANSCH;
        int index = 0 ;
        for (int i = 0 ; i < chs.length ; i++){
            if(chs[i].equals(ch)){
                index = i ;
            }
        }
        return PrintRes.TRANSEN[index] ;
    }

    public static String en2ch(String en){
        String[] chs = PrintRes.TRANSEN;
        int index = 0 ;
        for (int i = 0 ; i < chs.length ; i++){
            if(chs[i].equals(en)){
                index = i ;
            }
        }
        return PrintRes.TRANSCH[index] ;
    }

    private void loadWebGif(){
        wvInsert = (WebView) findViewById(R.id.webview_insert);
        wvInsert.loadDataWithBaseURL(null,"<HTML><body bgcolor='#FFF'><div align=center>" +
                "<img width=\"128\" height=\"128\" src='file:///android_asset/gif/dyn_insert.gif'/></div></body></html>", "text/html", "UTF-8",null);
        wvSwipe = (WebView) findViewById(R.id.webview_swipe);
        wvSwipe.loadDataWithBaseURL(null,"<HTML><body bgcolor='#FFF'><div align=center>" +
                "<img width=\"128\" height=\"128\" src='file:///android_asset/gif/dyn_swipe.gif'/></div></body></html>", "text/html", "UTF-8",null);
        wvPat = (WebView) findViewById(R.id.webview_pat);
        wvPat.loadDataWithBaseURL(null,"<HTML><body bgcolor='#FFF'><div align=center>" +
                "<img width=\"128\" height=\"128\" src='file:///android_asset/gif/dyn_pat.gif'/></div></body></html>", "text/html", "UTF-8",null);
    }

    private void showConfirmCardNO(String pan){
        btnConfirm = (Button) findViewById(R.id.cardno_confirm);
        btnCancel = (Button) findViewById(R.id.cardno_cancel);
        editCardNO = (EditText) findViewById(R.id.cardno_display_area);
        ImageView iv = (ImageView)findViewById(R.id.trans_cardno_iv) ;
        iv.setImageBitmap(PAYUtils.getLogoByBankId(this , TMConfig.getInstance().getBankid()));
        btnCancel.setOnClickListener(MasterControl.this);
        btnConfirm.setOnClickListener(MasterControl.this);
        editCardNO.setText(pan);
    }

    private void showOrignalTransInfo(TransLogData data){
        btnConfirm = (Button) findViewById(R.id.transinfo_confirm);
        btnCancel = (Button) findViewById(R.id.transinfo_cancel);
        btnCancel.setOnClickListener(MasterControl.this);
        btnConfirm.setOnClickListener(MasterControl.this);
        transInfo = (EditText) findViewById(R.id.transinfo_display_area);
        String info = getString(R.string.void_original_trans)+data.getEName()+"\n" ;
        if(data.isScan()){
            info += getString(R.string.void_pay_code)+data.getPan()+"\n" ;
        }else {
            info += getString(R.string.void_card_no)+data.getPan()+"\n" ;
        }
        info += getString(R.string.void_trace_no)+data.getTraceNo()+"\n" ;
        if(!PAYUtils.isNullWithTrim(data.getAuthCode())){
            info += getString(R.string.void_auth_code)+data.getAuthCode()+"\n";
        }
        info += getString(R.string.void_batch_no)+data.getBatchNo()+"\n";
        info += getString(R.string.void_amount)+PAYUtils.getStrAmount(data.getAmount())+"\n";
        info += getString(R.string.void_time)+PAYUtils.printStr(data.getLocalDate(), data.getLocalTime());
        transInfo.setText(info);
    }

    private void showHanding(String msg){
        TextView tv = (TextView) findViewById(R.id.handing_msginfo);
        tv.setText(msg);
        ImageView iv = (ImageView) findViewById(R.id.handing_img);
        iv.setImageBitmap(PAYUtils.getLogoByBankId(this , TMConfig.getInstance().getBankid()));
        WebView wv = (WebView) findViewById(R.id.handling_loading);
        wv.loadDataWithBaseURL(null,"<HTML><body bgcolor='#FFF'><div align=center>" +
                "<img width=\"128\" height=\"128\" src='file:///android_asset/gif/loading.gif'/></div></body></html>", "text/html", "UTF-8",null);
    }

    //add by vincent  ,for app choose
    private void showAppListInfo(String[] apps){
        btnConfirm = (Button) findViewById(R.id.appchoose_confirm);
        btnCancel = (Button) findViewById(R.id.appchoose_cancel);
        btnCancel.setOnClickListener(MasterControl.this);
        btnConfirm.setOnClickListener(MasterControl.this);
        RadioButton[] radioButtons = new RadioButton[apps.length];
        appId = new int[3];
        for (int i =0;i<apps.length;i++){
            Logger.debug("app info : "+apps[i]);
            if (apps[i].contains("PBOC")){//A000000333
                Logger.debug("car has Union application,add to view");
                radioButtons[i]= (RadioButton) findViewById(R.id.appUnionId);
                radioButtons[i].setVisibility(View.VISIBLE);
                radioButtons[i].setChecked(true);
                radioButtons[i].setOnClickListener(MasterControl.this);
                appId[0] = i;
            } else if (apps[i].contains("VISA")){//A0000000003
                Logger.debug("car has Visa application,add to view");
                radioButtons[i]= (RadioButton) findViewById(R.id.appVisaId);
                radioButtons[i].setVisibility(View.VISIBLE);
                radioButtons[i].setOnClickListener(MasterControl.this);
                appId[1] = i;
            }else if (apps[i].contains("MSKA")){//A0000000004
                Logger.debug("car has MSKA application,add to view");
                radioButtons[i]= (RadioButton) findViewById(R.id.appMSKAId);
                radioButtons[i].setVisibility(View.VISIBLE);
                radioButtons[i].setOnClickListener(MasterControl.this);
                appId[2] = i;
            }
        }
    }

    //add by vincent
    @Override
    protected void onDestroy() {
        super.onDestroy();
        CardManager.getInstance(FinanceTrans.INMODE_IC|
                FinanceTrans.INMODE_MAG|FinanceTrans.INMODE_NFC).releaseAll();
    }
}







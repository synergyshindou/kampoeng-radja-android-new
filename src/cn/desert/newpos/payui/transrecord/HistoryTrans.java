package cn.desert.newpos.payui.transrecord;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.android.newpos.pay.R;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.pinpad.OfflineRSA;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.printer.PrintManager;
import com.newpos.libpay.device.scanner.QRCInfo;
import com.newpos.libpay.presenter.TransUI;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.PAYUtils;

import java.util.ArrayList;
import java.util.List;

import cn.desert.newpos.payui.base.PayApplication;

public class HistoryTrans extends Activity implements
        HistorylogAdapter.OnItemReprintClick,TransUI{

    ListView lv_trans ;
    View view_nodata ;
    View view_reprint ;
    EditText search_edit ;
    ImageView search ;
    LinearLayout z ;
    LinearLayout root ;

    private HistorylogAdapter adapter;
    private boolean isSearch = false ;
    public static final String EVENTS = "EVENTS" ;
    public static final String LAST = "LAST" ;
    public static final String COMMON = "COMMON" ;
    public static final String ALL = "ALL" ;
    private boolean isCommonEvents = false ;
    private PrintManager manager = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        PayApplication.getInstance().addActivity(this);
        lv_trans = (ListView) findViewById(R.id.history_lv);
        view_nodata = findViewById(R.id.history_nodata);
        view_reprint = findViewById(R.id.reprint_process);
        search_edit = (EditText) findViewById(R.id.history_search_edit);
        search = (ImageView) findViewById(R.id.history_search);
        z = (LinearLayout) findViewById(R.id.history_search_layout);
        root = (LinearLayout) findViewById(R.id.transaction_details_root);
        adapter = new HistorylogAdapter(this , this) ;
        lv_trans.setAdapter(adapter);
        view_reprint.setVisibility(View.GONE);
        search.setOnClickListener(new SearchListener());
        manager = PrintManager.getmInstance(this , this);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            String even = bundle.getString(HistoryTrans.EVENTS);
            if(even.equals(LAST)){
                re_print(TransLog.getInstance().getLastTransLog());
            }else if(even.equals(ALL)){
                printAll();
            }else {
                isCommonEvents = true ;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    private void loadData() {
        List<TransLogData> list = TransLog.getInstance().getData();
        List<TransLogData> temp = new ArrayList<>() ;
        int num = 0 ;
        for (int i = list.size()-1  ; i >= 0 ; i--){
            temp.add(num , list.get(i));
            num++;
        }
        if (list.size() > 0) {
            showView(false);
            adapter.setList(temp);
            adapter.notifyDataSetChanged();
            isSearch = true ;
            search.setImageResource(android.R.drawable.ic_menu_search);
        } else {
            showView(true);
        }
    }

    private void showView(boolean isShow) {
        if (isShow) {
            lv_trans.setVisibility(View.GONE);
            view_nodata.setVisibility(View.VISIBLE);
        } else {
            lv_trans.setVisibility(View.VISIBLE);
            view_nodata.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnItemClick(String traceNO) {
        re_print(TransLog.getInstance().searchTransLogByTraceNo(traceNO));
    }

    private void re_print(final TransLogData data){
        view_reprint.setVisibility(View.VISIBLE);
        lv_trans.setVisibility(View.GONE);
        z.setVisibility(View.GONE);
        new Thread(){
            @Override
            public void run() {
                manager.printTest();
                //manager.print(data, true);
                mHandler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void printAll(){
        new Thread(){
            @Override
            public void run() {
                manager.printTest();
                //manager.printDetails();
                mHandler.sendEmptyMessage(0);
            }
        }.start();
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            view_reprint.setVisibility(View.GONE);
            lv_trans.setVisibility(View.VISIBLE);
            z.setVisibility(View.VISIBLE);
            if(!isCommonEvents){
                finish();
            }
        }
    };

    private final class SearchListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            if(isSearch){
                String edit = search_edit.getText().toString() ;
                if(!PAYUtils.isNullWithTrim(edit)){
                    TransLog transLog = TransLog.getInstance() ;
                    TransLogData data = transLog.searchTransLogByTraceNo(edit);
                    if(data != null){
                        InputMethodManager imm = (InputMethodManager) HistoryTrans.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search_edit.getWindowToken(),0);
                        List<TransLogData> list = new ArrayList<>() ;
                        list.add(0 , data);
                        adapter.setList(list);
                        adapter.notifyDataSetChanged();
                        search.setImageResource(android.R.drawable.ic_menu_revert);
                        isSearch = false ;
                    }else {
                        Toast.makeText(HistoryTrans.this ,
                                HistoryTrans.this.getResources().getString(R.string.not_any_record) ,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }else {
                loadData();
            }
        }
    }

    @Override
    public void handling(int timeout, final int status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lv_trans.setVisibility(View.GONE);
                z.setVisibility(View.GONE);
                view_reprint.setVisibility(View.VISIBLE);
                ((TextView)view_reprint.findViewById(R.id.handing_msginfo))
                        .setText(getResources().getString(R.string.re_print));
                ((WebView) findViewById(R.id.handling_loading)).
                        loadDataWithBaseURL(null,"<HTML><body bgcolor='#FFF'><div align=center>" +
                                "<img width=\"128\" height=\"128\" src='file:///android_asset/gif/loading.gif'/>" +
                                "</div></body></html>", "text/html", "UTF-8",null);
            }
        });
    }

    @Override
    public int showCardConfirm(int timeout, String cn) {
        return 0;
    }

    @Override
    public int showCardApplist(int timeout, String[] list) {
        return 0;
    }

    @Override
    public int showMultiLangs(int timeout, String[] langs) {
        return 0;
    }


    @Override
    public int showTransInfo(int timeout, TransLogData logData) {
        return 0;
    }

    @Override
    public void trannSuccess(int timeout, int code, String... args) {

    }

    @Override
    public void showError(int timeout, int errcode) {

    }

    @Override
    public InputInfo getOutsideInput(int i, InputManager.Mode mode) {
        return null;
    }

    @Override
    public CardInfo getCardUse(int i, int i1) {
        return null;
    }

    @Override
    public QRCInfo getQRCInfo(int i, InputManager.Style style) {
        return null;
    }

    @Override
    public PinInfo getPinpadOnlinePin(int i, String s, String s1) {
        return null;
    }

    @Override
    public PinInfo getPinpadOfflinePin(int i, int i1 , OfflineRSA rsaPinKey, int i2) {
        return null;
    }
}

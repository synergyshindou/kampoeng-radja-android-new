package com.android.newpos.pay;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.newpos.pay.model.TerminalAllocation;
import com.newpos.libpay.device.printer.PrintManager;
import com.pos.device.config.DevConfig;

import cn.desert.newpos.payui.base.PayApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.desert.newpos.payui.base.PayApplication.restApi;
import static cn.desert.newpos.payui.base.PayApplication.session;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        doAllocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.exit) {//add the function to perform here
            showConfirmDialogLogout();
            return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void showConfirmDialogLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setCancelable(false);
        builder.setMessage("Apakah anda yakin ingin logout akun?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PayApplication.getInstance().signOut(MainActivity.this);
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    public void doTopup(View view) {
        startActivity(new Intent(this, TopupActivity.class));
    }

    public void doTiket(View view) {
        startActivity(new Intent(this, TicketingActivity.class));
    }

    public void doRefund(View view) {
        startActivity(new Intent(this, RefundActivity.class));
    }

    public void doInfoCard(View view) {
        startActivity(new Intent(this, InfoKartuActivity.class));
    }

    // Not in Menu
    public void doCardPerso(View view) {
        startActivity(new Intent(this, CardPersoActivity.class));
    }

    public void doWahana(View view) {
        startActivity(new Intent(this, WahanaActivity.class));
    }

    public void doOutlet(View view) {
        startActivity(new Intent(this, OutletActivity.class));
    }

    public void doParkir(View view) {
        startActivity(new Intent(this, ParkingActivity.class));
    }

    void doAllocation() {
        restApi.getTerminalAllocation(DevConfig.getSN(), session.getAccessToken()).enqueue(new Callback<TerminalAllocation>() {
            @Override
            public void onResponse(Call<TerminalAllocation> call, Response<TerminalAllocation> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String t = response.body().getAllocationType();
                        Log.i(TAG, "onResponse: Allocation type " + t);
                        if (t.equals("THEMEPARK_IN_GATE")) {
                            Log.i(TAG, "onResponse: Nothing to do here");
                        } else if (t.equalsIgnoreCase("VISITOR")) {
                            Intent intent = new Intent(MainActivity.this, InfoKartuActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("CARD_ACTIVATION")) {
                            Intent intent = new Intent(MainActivity.this, CardPersoActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("ATTRACTION_IN_GATE")) {
                            Intent intent = new Intent(MainActivity.this, WahanaActivity.class);
                            intent.putExtra("ATR", response.body().getAttractionName());
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("PARKING_IN_GATE")) {
                            Intent intent = new Intent(MainActivity.this, ParkingActivity.class);
                            intent.putExtra("TYPES", 1);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("PARKING_OUT_GATE")) {
                            Intent intent = new Intent(MainActivity.this, ParkingActivity.class);
                            intent.putExtra("TYPES", 2);
                            startActivity(intent);
                            finish();
                        } else if (t.equalsIgnoreCase("OUTLET")) {
                            Intent intent = new Intent(MainActivity.this, OutletActivity.class);
                            intent.putExtra("KODE", response.body().getOutletCode());
                            intent.putExtra("NAMA", response.body().getOutletName());
                            Log.e(TAG, "onResponse: " + response.body().getOutletName());
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<TerminalAllocation> call, Throwable t) {

            }
        });
    }

    public void doPrint() {
        new Thread() {
            @Override
            public void run() {
                PrintManager.getmInstance(PayApplication.getInstance()).printTest();
            }
        }.start();
    }
}

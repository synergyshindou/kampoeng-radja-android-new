package com.android.newpos.pay.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}

package com.android.newpos.pay.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.android.newpos.pay.model.OutletItem;
import com.android.newpos.pay.repo.OutletRepository;

import java.util.List;

public class OutletViewModel extends AndroidViewModel {

    private OutletRepository repository;
    private LiveData<List<OutletItem>> allOutletItems;

    public OutletViewModel(@NonNull Application application) {
        super(application);
        repository = new OutletRepository(application);
        allOutletItems = repository.getAllOutletItems();
    }

    public void insert(OutletItem note){
        repository.insert(note);
    }
    public void update(OutletItem note){
        repository.update(note);
    }
    public void delete(OutletItem note){
        repository.delete(note);
    }
    public void deleteAllOutletItems(){
        repository.deleteAllOutletItems();
    }

    public LiveData<List<OutletItem>> getAllOutletItems(){
        return allOutletItems;
    }

    public List<OutletItem> getTiketById(int itemId){
        return repository.getAllTiketById(itemId);
    }
}

package com.android.newpos.pay;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.desert.admanager.ConvenientBanner;
import com.android.desert.admanager.holder.CBViewHolderCreator;
import com.android.desert.baserecyle.BaseQuickAdapter;
import com.android.desert.baserecyle.adapter.HomeAdapter;
import com.android.desert.baserecyle.entity.HomeItem;
import com.newpos.libpay.Logger;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.device.printer.PrintManager;
import com.newpos.libpay.device.printer.PrintRes;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.translog.TransLog;

import cn.desert.newpos.payui.simple.AdHolder;
import cn.desert.newpos.payui.IItem;
import cn.desert.newpos.payui.UIUtils;
import cn.desert.newpos.payui.base.PayApplication;
import cn.desert.newpos.payui.master.MasterControl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.desert.newpos.payui.setting.ui.SettingsFrags;
import cn.desert.newpos.payui.simple.SecondMenu;
import cn.desert.newpos.payui.transrecord.HistoryTrans;

/**
 * @author zhouqiang
 * @email wy1376359644@163.com
 */
public class HomeActivity extends AppCompatActivity implements
        BaseQuickAdapter.OnItemClickListener , View.OnClickListener{

    /**
     * 经典桌面
     */
    private ArrayList<HomeItem> mDataList;
    private RecyclerView mRecyclerView;
    private BaseQuickAdapter homeAdapter ;
    private boolean refreash = false ;
    private long mkeyTime = 0 ;

    private static String[] HOMES = null ;
    private static String[] ENQUIRYS = null ;
    private static String[] PREAUTHS = null ;
    private static String[] MANAGERS = null ;


    private Dialog secondMenuDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PaySdk.getInstance().setActivity(this);
        PayApplication.getInstance().addActivity(this);
        PayApplication.getInstance().setRunned();
    }

    private void initSimple(){
        setContentView(R.layout.home_simple);
        findViewById(R.id.scan).setOnClickListener(Home2ClickListener);
        findViewById(R.id.print).setOnClickListener(Home2ClickListener);
        LinearLayout manager = (LinearLayout) findViewById(R.id.manager) ;
        manager.setOnClickListener(Home2ClickListener);
        findViewById(R.id.cash_iv).setOnClickListener(Home2ClickListener);
        LinearLayout revocation = (LinearLayout) findViewById(R.id.revocation) ;
        revocation.setOnClickListener(Home2ClickListener);
        findViewById(R.id.others).setOnClickListener(Home2ClickListener);
        findViewById(R.id.test_print).setOnClickListener(Home2ClickListener);
        if(Locale.getDefault().getLanguage().equals("zh")){
            revocation.setBackground(getDrawable(R.drawable.home2_void));
            manager.setBackground(getDrawable(R.drawable.home2_manager));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initSimple();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if(refreash){
                refreash = false ;
                initHomeView(getResources().getStringArray(R.array.trans) , IItem.Home.imgs);
            }else {
                if ((System.currentTimeMillis() - mkeyTime) > 2000) {
                    mkeyTime = System.currentTimeMillis();
                    Toast.makeText(this , getString(R.string.app_exit) , Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                }
            }
            return true ;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        String text = ((TextView)view.findViewById(R.id.text)).getText().toString().trim();
        if(text.equals(IItem.Menus.ENQUIRY)){
            refreash = true ;
            initHomeView(ENQUIRYS,IItem.Home_Enquiry.imgs);
        }else if(text.equals(IItem.Menus.PREAUTH)){
            refreash = true ;
            initHomeView(PREAUTHS,IItem.Home_Preauth.imgs);
        }else if(text.equals(IItem.Menus.MANAGER)){
            refreash = true ;
            initHomeView(MANAGERS,IItem.Home_Manage.imgs);
        }else if(text.equals(IItem.Menus.APPSTORE)){
            if(PayApplication.getInstance().isAppInstalled(this , "com.android.rong.appstore")){
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.android.rong.appstore",
                        "com.android.rong.appstore.MainActivity"));
                intent.setAction(Intent.ACTION_VIEW);
                startActivity(intent);
            }else {
                Toast.makeText(this , getString(R.string.appstore_not_install) , Toast.LENGTH_SHORT).show();
            }
        }else if(text.equals(IItem.Menus.PROSETTING)){
            Intent intent = new Intent();
            intent.setClass(this , SettingsFrags.class);
            startActivity(intent);
        }else if(text.equals(IItem.Menus.SYSSETTING)){
            Intent intent = new Intent(Settings.ACTION_SETTINGS);
            startActivity(intent);
        }else if(text.equals(IItem.Menus.MANAGER_5)){

        }else{
            startTrans(text);
        }
    }

    @Override
    public void onClick(View view) {
        startTrans(PrintRes.TRANSCH[1]);
    }

    private void initHomeView(String[] TITLES , int[] IMGS) {
        mDataList = new ArrayList<>();
        for (int i = 0; i < TITLES.length; i++) {
            HomeItem item = new HomeItem();
            item.setTitle(TITLES[i]);
            item.setImageResource(IMGS[i]);
            mDataList.add(item);
        }
        homeAdapter = new HomeAdapter(R.layout.home_item_view, mDataList);
        homeAdapter.openLoadAnimation();
        View top = getLayoutInflater().inflate(R.layout.home_top_view, (ViewGroup) mRecyclerView.getParent(), false);
        top.findViewById(R.id.topview).setOnClickListener(this);
        homeAdapter.addHeaderView(top);
        homeAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(homeAdapter);
    }

    private void startTrans(String ch){
        String en = MasterControl.ch2en(ch);
        Logger.debug("startTrans:" + en);
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClass(this , MasterControl.class);
        intent.putExtra(MasterControl.TRANS_KEY , ch);
        startActivity(intent);
    }

    private final View.OnClickListener Home2ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.cash_iv){
                startTrans(PrintRes.TRANSCH[1]);
            } else
            if(view.getId() == R.id.revocation){
                startTrans(PrintRes.TRANSCH[2]);
            } else
            if(view.getId() == R.id.test_print){
                new Thread() {
                    @Override
                    public void run() {
                        PrintManager.getmInstance(PayApplication.getInstance()).printTest();
                    }
                }.start();
            } else {
                String[] items = null ;
                List<HashMap<String , Object>> list = new ArrayList<>();
                int[] ids = null ;
                switch (view.getId()){
                    case R.id.scan:
                        ids = SecondMenu.SCAN_;
                        items = HomeActivity.this.getResources().getStringArray(R.array.scan_);
                        break;

                    case R.id.print:
                        ids = SecondMenu.PRINT_ ;
                        items = HomeActivity.this.getResources().getStringArray(R.array.print_);
                        break;
                    case R.id.manager:
                        ids = SecondMenu.MANA_;
                        items = HomeActivity.this.getResources().getStringArray(R.array.mana_);
                        break;
                    case R.id.others:
                        ids = SecondMenu.OTHERS_ ;
                        items = HomeActivity.this.getResources().getStringArray(R.array.others_);
                        break;
                }
                for (int i = 0 ; i < items.length ; i++){
                    HashMap<String , Object> map = new HashMap<>();
                    map.put(SecondMenu.IVKEY , ids[i]);
                    map.put(SecondMenu.TVKEY , items[i]);
                    list.add(map);
                }
                displaySecondMenu(list);
            }
        }
    };

    public void close_second_menu(View v){
        secondMenuDialog.dismiss();
    }

    private void displaySecondMenu(List<HashMap<String , Object>> list){
        secondMenuDialog = new Dialog(this, R.style.Translucent_Dialog);
        secondMenuDialog.setContentView(R.layout.home_simple_second_menu);
        RelativeLayout layout = (RelativeLayout) secondMenuDialog.findViewById(R.id.r_dialog);
        GridView gridView = (GridView) layout.findViewById(R.id.second_menu_gv);
        gridView.setNumColumns(list.size());
        gridView.setAdapter(new SimpleAdapter(this , list , R.layout.home_simple_second_menu_item ,
                 new String[]{SecondMenu.IVKEY , SecondMenu.TVKEY} ,
                 new int[]{R.id.second_menu_item_iv , R.id.second_menu_item_tv}));
        gridView.setOnItemClickListener(new SecondMenuListener());
        secondMenuDialog.show();
    }

    private final class SecondMenuListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            secondMenuDialog.dismiss();
            String text = ((TextView)view.findViewById(R.id.second_menu_item_tv)).getText().toString();
            Logger.debug("get original text : "+text);
            if(!Locale.getDefault().getLanguage().equals("zh")){
                for (int k = 0 ; k < IItem.MenusEN.length ; k++){
                    if(text.equals(IItem.MenusEN[k])){
                        text = IItem.MenusCN[k] ;
                        break;
                    }
                }
            }
            if(text.equals(IItem.Menus.QUERYTRANSDETAILS)){
                Intent intent = new Intent(HomeActivity.this , HistoryTrans.class);
                intent.putExtra(HistoryTrans.EVENTS , HistoryTrans.COMMON);
                startActivity(intent);
            }else if(text.equals(IItem.Menus.REPRINTLAST)){
                if(TransLog.getInstance().getSize()>0){
                    Intent intent = new Intent(HomeActivity.this , HistoryTrans.class);
                    intent.putExtra(HistoryTrans.EVENTS , HistoryTrans.LAST);
                    startActivity(intent);
                }else {
                    Toast.makeText(HomeActivity.this , getResources().getString(R.string.not_any_record) ,
                            Toast.LENGTH_SHORT).show();
                }
            }else if(text.equals(IItem.Menus.REPRINTALL)){
                if(TransLog.getInstance().getSize()>0){
                    Intent intent = new Intent(HomeActivity.this , HistoryTrans.class);
                    intent.putExtra(HistoryTrans.EVENTS , HistoryTrans.ALL);
                    startActivity(intent);
                }else {
                    Toast.makeText(HomeActivity.this , getResources().getString(R.string.not_any_record) ,
                            Toast.LENGTH_SHORT).show();
                }
            }else if(text.equals(IItem.Menus.PROSETTING)){
                startActivity(new Intent(HomeActivity.this , SettingsFrags.class));
            }else {
                Logger.debug("SecondMenuListener text : "+text);
                startTrans(text);
            }
        }
    }
}

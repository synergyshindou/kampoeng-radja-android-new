package com.android.newpos.pay;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.newpos.pay.adapter.OutletAdapter;
import com.android.newpos.pay.model.ItemCategory;
import com.android.newpos.pay.model.OutletItem;
import com.android.newpos.pay.utils.Method;
import com.android.newpos.pay.viewmodel.OutletViewModel;
import com.android.newpos.pay.widgets.ElegantNumberButton;
import com.nex3z.notificationbadge.NotificationBadge;
import com.pos.device.scanner.OnScanListener;
import com.pos.device.scanner.Scanner;

import java.util.ArrayList;
import java.util.List;

import cn.desert.newpos.payui.base.PayApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.desert.newpos.payui.base.PayApplication.restApi;
import static cn.desert.newpos.payui.base.PayApplication.session;


public class OutletActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int mCount = 0, back = 0;
    Toolbar toolbar;
    NotificationBadge badge;
    View parent_view, rlKeranjang, llMore;
    AppCompatButton btnDetail, btnKeranjang;

    String num = "", sCode = "", sNama = "";
    private static final String TAG = "OutletActivity";

    LinearLayout llList;

    List<ItemCategory> itemCategories;

    List<OutletItem> listCount;

    OutletViewModel outletViewModel;

    OutletAdapter adapterGlobal;
    List<OutletItem> listSearch = new ArrayList<>();

    RecyclerView rvMore, rvGlobal;
    private static final int CALLBACK_SCANS = 4;
    private static final int INIT_SCANS = 5;
    private static final int START_SCANS = 6;
    private static final int STOP_SCANS = 7;
    private static final int STOP_SCANS_TIMEOUT = 8;
    private static final int STOP_SCANS_OTHER = 9;
    private static final int CHANGE_CAMERA = 10;

    private RelativeLayout previewLayout;
    private View cameraPreviewScanS = null;
    EditText etsearch;
    NestedScrollView nested_scroll_view;
    Dialog dialog;

    SearchView searchView;

    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);
        if (getIntent().hasExtra("KODE"))
            sCode = getIntent().getStringExtra("KODE");
        if (getIntent().hasExtra("NAMA"))
            sNama = getIntent().getStringExtra("NAMA");
        Log.e(TAG, "onCreate: OA " + sNama);

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_tiket_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        initView();
        getAllCategories();

        outletViewModel = ViewModelProviders.of(this).get(OutletViewModel.class);
        outletViewModel.getAllOutletItems().observe(this, new Observer<List<OutletItem>>() {
            @Override
            public void onChanged(@Nullable List<OutletItem> notes) {
                if (notes != null) {
                    int sum = 0;
                    for (int i = 0; i < notes.size(); i++) {
                        sum += notes.get(i).getQuantity();
                    }
                    badge.setNumber(sum);
                }
                listCount = notes;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.exit) {//add the function to perform here
            showConfirmDialogLogout();
            return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void showConfirmDialogLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setCancelable(false);
        builder.setMessage("Apakah anda yakin ingin logout akun?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PayApplication.getInstance().signOut(OutletActivity.this);
                outletViewModel.deleteAllOutletItems();
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        if (back == 1) {
            llList.setVisibility(View.VISIBLE);
            llMore.setVisibility(View.GONE);
            back = 0;
        } else if (back == 2){
            stopScanS();
            back = 0;
        } else
            super.onBackPressed();
    }

    public void startScans(View view) {

//        int menit = 1 * 60;
//        handler.sendMessage(handler.obtainMessage(START_SCANS, menit));

        if (back == 0) {
            handler.sendEmptyMessage(INIT_SCANS);
            back = 2;
        } else {
            Snackbar.make(parent_view, "Sedang dalam mode scan", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
        }
    }

    void initView() {
        badge = findViewById(R.id.badge);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Outlet "+sNama);

        setSupportActionBar(toolbar);

        parent_view = findViewById(R.id.parent_view);

        rlKeranjang = findViewById(R.id.rlKeranjang);

        rlKeranjang.setOnClickListener(this);

        llList = findViewById(R.id.llList);
        llMore = findViewById(R.id.llMore);

        rvMore = findViewById(R.id.rvPaketMore);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(OutletActivity.this, LinearLayoutManager.VERTICAL, false);
        rvMore.setLayoutManager(horizontalLayoutManagaer);

        rvGlobal = findViewById(R.id.rvGlobal);
        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(OutletActivity.this, LinearLayoutManager.VERTICAL, false);
        rvGlobal.setLayoutManager(horizontalLayoutManager);

        etsearch = findViewById(R.id.et_search);
        previewLayout = findViewById(R.id.capture_preview);
        nested_scroll_view = findViewById(R.id.nested_scroll_view);

        adapterGlobal = new OutletAdapter(OutletActivity.this, listSearch, new OutletAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(OutletItem item) {
                Log.i(TAG, "onItemClick: " + item.getTitle());
                showDialogItem(item);
            }
        });

        rvGlobal.setAdapter(adapterGlobal);

        searchView = findViewById(R.id.searchViewTool);
        setupSearchView();

        tvTitle = findViewById(R.id.tvTitleMore);
    }

    public void setupSearchView()
    {
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Cari Makanan/Minuman");
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CALLBACK_SCANS:
                    try {
                        byte[] decodeByte = (byte[]) msg.obj;
                        Log.i(TAG, "handleMessage: CallBACK " + new String(decodeByte));
                        doSearchItem(new String(decodeByte));
                        stopScanS();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case INIT_SCANS:
                    try {
                        Bundle realBundle = new Bundle();
                        realBundle.putBoolean(Scanner.SCANNER_CONTINUE_SCAN, true);
                        realBundle.putBoolean(Scanner.SCANNER_IS_BACK_CAMERA, true);
                        realBundle.putBoolean(Scanner.SCANNER_PLAY_BEEP, false);
                        realBundle.putBoolean(Scanner.SCANNER_IS_TORCH_ON, true);
                        cameraPreviewScanS = Scanner.getInstance().initScanner(OutletActivity.this, realBundle);
                        startScanS(60);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case START_SCANS:
                    int timeoutScanS = (Integer) msg.obj;
                    startScanS(timeoutScanS);
                    break;
                case STOP_SCANS:
                    Log.w(TAG, "handleMessage: STOP_SCANS " + "".getBytes());
                    stopScanS();
                    break;
                case STOP_SCANS_TIMEOUT:
                    Log.w(TAG, "handleMessage: STOP_SCANS_TIMEOUT " + "".getBytes());
                    stopScanS();
                    break;
                case STOP_SCANS_OTHER:

                    Log.w(TAG, "handleMessage: STOP_SCANS_OTHER " + "".getBytes());
                    stopScanS();
                    break;
                case CHANGE_CAMERA:
                    Scanner.getInstance().stopScan();
                    Bundle b = new Bundle();
                    b.putBoolean(Scanner.SCANNER_CONTINUE_SCAN, true);
                    b.putBoolean(Scanner.SCANNER_PLAY_BEEP, false);
                    b.putBoolean(Scanner.SCANNER_IS_TORCH_ON, true);
                    try {
                        cameraPreviewScanS = Scanner.getInstance().initScanner(OutletActivity.this, b);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void stopScanS() {
        try {
            if (previewLayout != null) {
                previewLayout.removeAllViews();
                nested_scroll_view.setVisibility(View.VISIBLE);
            }
            Scanner.getInstance().stopScan();
        } catch (Exception e) {
            e.printStackTrace();
        }
        back = 0;
    }

    private void startScanS(int timeout) {
        try {
            if (previewLayout != null && cameraPreviewScanS != null) {
                previewLayout.removeAllViews();
                previewLayout.addView(cameraPreviewScanS);
                nested_scroll_view.setVisibility(View.GONE);
            }
            Scanner.getInstance().startScan(timeout, new OnScanListener() {
                @Override
                public void onScanResult(int result, byte[] data) {
                    try {
                        if (result == 0) {
                            System.out.println("data：" + new String(data));
                            handler.sendMessage(handler.obtainMessage(CALLBACK_SCANS, data));
                        } else if (result == -1) {
                            System.out.println("STOP SCAN");
                            handler.sendMessage(handler.obtainMessage(STOP_SCANS));
                        } else if (result == -3) {
                            System.out.println("TIMEOUT");
                            handler.sendMessage(handler.obtainMessage(STOP_SCANS_TIMEOUT));
                        } else {
                            System.out.println("SCAN OTHER");
                            handler.sendMessage(handler.obtainMessage(STOP_SCANS_OTHER));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getAllCategories() {
        restApi.getAllCategories(session.getAccessToken()).enqueue(new Callback<List<ItemCategory>>() {
            @Override
            public void onResponse(Call<List<ItemCategory>> call, Response<List<ItemCategory>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        itemCategories = response.body();
                        for (int i = 0; i < itemCategories.size(); i++) {
                            getOutletBy(itemCategories.get(i).getValue(), itemCategories.get(i).getLabel(), i);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ItemCategory>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    void getOutletBy(String itemCategory, final String label, final int ke) {
        restApi.getOutletItemByCategory(itemCategory, sCode, 0, 100, session.getAccessToken()).enqueue(new Callback<List<OutletItem>>() {
            @Override
            public void onResponse(Call<List<OutletItem>> call, Response<List<OutletItem>> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        OutletAdapter outletAdapter = new OutletAdapter(OutletActivity.this, response.body(), new OutletAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(OutletItem item) {
                                Log.i(TAG, "onItemClick: " + item.getTitle());
                                showDialogItem(item);
                            }
                        });
                        initItem(label, outletAdapter, ke);
                        listSearch.addAll(response.body());
                        adapterGlobal.notifyDataSetChanged();

                    } else {
                        Log.e(TAG, "onResponse: " + response.errorBody());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<OutletItem>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void doChart() {
        if (listCount.size() > 0) {
            Intent intent = new Intent(this, KeranjangOutletActivity.class);
            Log.e(TAG, "doChart: " + sNama);
            intent.putExtra("NAMA", sNama);
            startActivity(intent);
        } else
            Snackbar.make(parent_view, "Keranjang masih kosong", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
    }

    public void doLoadMore(String v, OutletAdapter outletAdapter) {
        back = 1;
        llList.setVisibility(View.GONE);
        llMore.setVisibility(View.VISIBLE);
        rvMore.setAdapter(outletAdapter);
        tvTitle.setText(v);
    }

    public void doSearchItem(String dats) {
        restApi.getOutletItemByCode(dats, sCode, session.getAccessToken()).enqueue(new Callback<OutletItem>() {
            @Override
            public void onResponse(Call<OutletItem> call, Response<OutletItem> response) {
                Log.e(TAG, "onResponse: " +response.isSuccessful() );
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse: Wadaw");
                    if (response.body() != null)
                        showDialogItem(response.body());
                    else
                        Snackbar.make(parent_view, "Barang tersebut tidak ada/belum terdaftar pada kios ini", Snackbar.LENGTH_INDEFINITE).setDuration(5000).show();

                } else {
                    Log.e(TAG, "onResponse: GAGAL SON");
                    Snackbar.make(parent_view, "Barang tersebut tidak ada/belum terdaftar pada kios ini", Snackbar.LENGTH_INDEFINITE).setDuration(5000).show();
                }
            }

            @Override
            public void onFailure(Call<OutletItem> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString() );
                Snackbar.make(parent_view, "Barang tersebut tidak ada/belum terdaftar pada kios ini", Snackbar.LENGTH_INDEFINITE).setDuration(5000).show();

            }
        });

    }

    public void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_outlet_keranjang);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
        button.setNumber("1");
        num = "1";
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = button.getNumber();
                Log.i(TAG, "onClick: " + num);
                if (button.getNumbers() == 0)
                    dialog.hide();
            }
        });

        AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar.make(parent_view, "Paket " + TAG + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                dialog.hide();
                badge.setNumber(Integer.parseInt(num));
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnKeranjang:
                showDialog();
                break;
            case R.id.rlKeranjang:
                doChart();
                break;
        }
    }

    void initItem(final String sTitle, final OutletAdapter outletAdapter, int ke) {
        LinearLayout layoutUtama = new LinearLayout(this);
        layoutUtama.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        if (ke == 0)
            params.setMargins(0, intToDp(15), 0, 0);

        layoutUtama.setLayoutParams(params);

        LinearLayout layoutT = new LinearLayout(this);
        layoutT.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        layoutT.setOrientation(LinearLayout.HORIZONTAL);
        layoutT.setPadding(intToDp(15), 0, intToDp(15), 0);

        TextView textView = new TextView(this);
        textView.setText(sTitle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAppearance(R.style.TextAppearance_AppCompat_Title);
        } else
            textView.setTextAppearance(this, R.style.TextAppearance_AppCompat_Title);

        LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
        textView.setLayoutParams(paramsTv);

        Button btn = new Button(new ContextThemeWrapper(this, R.style.Widget_AppCompat_Button_Borderless), null, 0);
        btn.setText("Lihat Semua");
        btn.setTextColor(getResources().getColor(R.color.grey_60));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLoadMore(sTitle, outletAdapter);
            }
        });

        layoutT.addView(textView);
        layoutT.addView(btn);

        int px = intToDp(5);

        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        recyclerView.setPadding(px, px, px, px);

        Log.i(TAG, "initItem: itemCount = " + outletAdapter.getItemCount());

        if (outletAdapter.getItemCount() > 0) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),
                    outletAdapter.getItemCount() < 3 ? outletAdapter.getItemCount() : 3);
            gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL); // set Horizontal Orientation
            recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView

            recyclerView.setAdapter(outletAdapter);
        }

        layoutUtama.addView(layoutT);
        layoutUtama.addView(recyclerView);

        llList.addView(layoutUtama);
    }

    public void showDialogItem(final OutletItem item) {
        Log.i(TAG, "showDialogItem: "+item.getTitle());
        if (dialog == null){
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_tiket_keranjang);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);
        }

            final TextView tvTitle, tvPrice, tvType, tvValid;

            tvTitle = dialog.findViewById(R.id.tvTitle);
            tvPrice = dialog.findViewById(R.id.tvPrice);
            tvType = dialog.findViewById(R.id.tvType);
            tvValid = dialog.findViewById(R.id.tvValid);

            tvTitle.setText(item.getTitle());
            tvPrice.setText(Method.getFormatIDRS(item.getPrice()));
            ImageView img = dialog.findViewById(R.id.image);
            if (item.getImagePath() != null) {
                // byte[] imageByteArray = Base64.decode(item.getImageBase64(), Base64.DEFAULT);
                // here imageBytes is base64String

//            Glide.with(OutletActivity.this)
//                    .load(Constants.BASE_URL_IMAGE + item.getImagePath())
//                    .into(img);
            }

            final ElegantNumberButton button = dialog.findViewById(R.id.number_button);
            button.setNumber("1");
            num = "1";
            button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                    num = button.getNumber();

                    Log.i(TAG, "onClick: " + num);
                    if (button.getNumbers() == 0)
                        dialog.dismiss();
                }
            });

            AppCompatButton btn = dialog.findViewById(R.id.bt_add_to_cart);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    List<OutletItem> getTiket = outletViewModel.getTiketById(item.getItemId());
                    if (getTiket != null) {
                        Log.i(TAG, "onClick: " + getTiket.size());

                        OutletItem tpr;
                        if (getTiket.size() == 1) {
                            tpr = getTiket.get(0);
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                            dialog.dismiss();

                            item.setQuantity(button.getNumbers() + tpr.getQuantity());
                            outletViewModel.update(item);
                        } else {
                            Log.e(TAG, "onClick: Size 0");
                            Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                            dialog.dismiss();

                            item.setQuantity(button.getNumbers());
                            outletViewModel.insert(item);
                        }

                    } else {
                        Log.i(TAG, "onClick: else ");
                        Snackbar.make(parent_view, tvTitle.getText().toString() + " berhasil masuk keranjang", Snackbar.LENGTH_INDEFINITE).setDuration(3000).show();
                        dialog.dismiss();

                        item.setQuantity(button.getNumbers());
                        outletViewModel.insert(item);
                    }
                }
            });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public int intToDp(int a) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, a, r.getDisplayMetrics()));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (adapterGlobal != null) {
            adapterGlobal.getFilter().filter(newText);
            if (newText.length() > 0) {
                rvGlobal.setVisibility(View.VISIBLE);
                llList.setVisibility(View.GONE);
            } else {
                rvGlobal.setVisibility(View.GONE);
                llList.setVisibility(View.VISIBLE);
            }
        }
        Log.i(TAG, "onQueryTextChange: "+newText);
        return false;
    }
}

package com.android.newpos.pay.rest;

import com.android.newpos.pay.model.AuthCardResp;
import com.android.newpos.pay.model.CalcPriceDetail;
import com.android.newpos.pay.model.CalculatePriceResp;
import com.android.newpos.pay.model.CardActivationReq;
import com.android.newpos.pay.model.CardIdentifierId;
import com.android.newpos.pay.model.CardInfoDataJava;
import com.android.newpos.pay.model.CardPersoResp;
import com.android.newpos.pay.model.CardRepersoResp;
import com.android.newpos.pay.model.CardTerminal;
import com.android.newpos.pay.model.DetailTicketResp;
import com.android.newpos.pay.model.ItemCategory;
import com.android.newpos.pay.model.OutletItem;
import com.android.newpos.pay.model.OutletPurchase;
import com.android.newpos.pay.model.OutletPurchaseRequest;
import com.android.newpos.pay.model.PaymentMethodResp;
import com.android.newpos.pay.model.RefundDetailReq;
import com.android.newpos.pay.model.RefundDetailResp;
import com.android.newpos.pay.model.RefundResp;
import com.android.newpos.pay.model.TerminalAllocation;
import com.android.newpos.pay.model.TicketPurchaseResp;
import com.android.newpos.pay.model.TicketingPurchaseRequest;
import com.android.newpos.pay.model.TiketParkirResp;
import com.android.newpos.pay.model.TopUpReq;
import com.android.newpos.pay.model.TopUpResp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiConfig {

    @GET("card-info")
    Call<CardInfoDataJava> cardInfo(@Query("cardUid") String cardUID,
                                    @Header("Authorization") String authHeader);

    @GET("card/identifierid/{cardUID}")
    Call<CardIdentifierId> getCardIdentifierId(@Path("cardUID") String cardUID,
                                               @Header("Authorization") String authHeader);

    @GET("terminal/allocation")
    Call<TerminalAllocation> getTerminalAllocation(@Query("terminalSerialNumber") String terminalSerialNumber,
                                                   @Header("Authorization") String authHeader);

    @GET("ticketing/parking-tickets")
    Call<List<TiketParkirResp>> parkirTicket(@Query("pageNumber") int pageNumber,
                                             @Query("pageSize") int pageSize,
                                             @Header("Authorization") String authHeader);

    @GET("ticketing/entrance-package-by-age-type")
    Call<List<TiketParkirResp>> packageByAge(@Query("ageType") String ageType,
                                             @Query("pageNumber") int pageNumber,
                                             @Query("pageSize") int pageSize,
                                             @Header("Authorization") String authHeader);

    @GET("ticketing/detail/{itemId}")
    Call<DetailTicketResp> getItemDetail(@Path("itemId") String itemId,
                                         @Header("Authorization") String authHeader);

    @GET("ticketing/payment/method")
    Call<List<PaymentMethodResp>> getPaymentMethod(
            @Header("Authorization") String authHeader);

    @GET("item/category")
    Call<List<ItemCategory>> getAllCategories(@Header("Authorization") String authHeader);

    @GET("outlet/outlet-item-by-category")
    Call<List<OutletItem>> getOutletItemByCategory(@Query("itemCategory") String itemCategory,
                                                   @Query("outletCode") String outletCode,
                                                   @Query("pageNumber") int pageNumber,
                                                   @Query("pageSize") int pageSize,
                                                   @Header("Authorization") String authHeader);

    @GET("outlet/outlet-item-by-code")
    Call<OutletItem> getOutletItemByCode(@Query("itemCode") String itemCode,
                                         @Query("outletCode") String outletCode,
                                         @Header("Authorization") String authHeader);

    @POST("topup")
    Call<TopUpResp> topUpRequest(@Body TopUpReq topUpReq,
                                 @Header("Authorization") String authHeader);

    @POST("card/perso")
    Call<CardPersoResp> cardPerso(@Body CardTerminal cardTerminal,
                                  @Header("Authorization") String authHeader);

    @POST("card/reperso")
    Call<CardRepersoResp> cardReperso(@Body CardTerminal cardTerminal,
                                      @Header("Authorization") String authHeader);

    @POST("card/activate")
    Call<Void> activeCard(@Body CardActivationReq cardActivationReq,
                          @Header("Authorization") String authHeader);

    @POST("auth/card")
    Call<AuthCardResp> authCard(@Body CardTerminal cardTerminal,
                                @Header("Authorization") String authHeader);

    @POST("ticketing/calculate-price-detail")
    Call<CalculatePriceResp> calculatePriceDetail(@Body CalcPriceDetail calcPriceDetail,
                                                  @Header("Authorization") String authHeader);

    @POST("ticketing/purchase")
    Call<TicketPurchaseResp> tiketPurchase(@Body TicketingPurchaseRequest ticketingPurchaseRequest,
                                           @Header("Authorization") String authHeader);

    @POST("outlet/purchase")
    Call<OutletPurchase> outletPurchase(@Body OutletPurchaseRequest outletPurchaseRequest,
                                        @Header("Authorization") String authHeader);

    @POST("refund/detail")
    Call<RefundDetailResp> refundDetail(@Body RefundDetailReq refundDetailReq,
                                        @Header("Authorization") String authHeader);

    @POST("refund")
    Call<RefundResp> refund(@Body RefundDetailReq refundDetailReq,
                            @Header("Authorization") String authHeader);

    @POST("parking/park-in")
    Call<Void> parkIn(@Body CardTerminal cardTerminal,
                      @Header("Authorization") String authHeader);

    @POST("parking/park-out")
    Call<Void> parkOut(@Body CardTerminal cardTerminal,
                       @Header("Authorization") String authHeader);

    @POST("attraction/access")
    Call<Void> wahanaAccess(@Body CardTerminal cardTerminal,
                            @Header("Authorization") String authHeader);

}
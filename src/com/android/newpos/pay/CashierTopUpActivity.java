package com.android.newpos.pay;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import cn.desert.newpos.payui.base.PayApplication;

public class CashierTopUpActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier_topup);
        setTitle("Cashier Topup");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.exit) {//add the function to perform here
            showConfirmDialogLogout();
            return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void showConfirmDialogLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Kampoeng Radja");
        builder.setCancelable(false);
        builder.setMessage("Apakah anda yakin ingin logout akun?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PayApplication.getInstance().signOut(CashierTopUpActivity.this);
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    public void doTopup(View view) {
        startActivity(new Intent(this, TopupActivity.class));
    }

    public void doInfoCard(View view) {
        startActivity(new Intent(this, InfoKartuActivity.class));
    }
}

package com.newpos.libpay.presenter;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.pinpad.OfflineRSA;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.scanner.QRCInfo;
import com.newpos.libpay.trans.translog.TransLogData;

/**
 * Created by zhouqiang on 2017/3/15.
 * @author zhouqiang
 * 交易UI接口类
 */

public interface TransUI {
    /**
     * 获取外界输入UI接口(提示用户输入信息)
     * @return
     */
    InputInfo getOutsideInput(int timeout, InputManager.Mode type);

    /**
     * 获取外界卡片UI接口(提示用户用卡)
     * @return
     */
    CardInfo getCardUse(int timeout, int mode);

    /**
     * 获取外界扫码支付方式接口(提示用户扫码操作)
     * @param timeout
     * @param mode @{@link InputManager.Style}
     * @return
     */
    QRCInfo getQRCInfo(int timeout, InputManager.Style mode);

    /**
     * 获取密码键盘输入联机PIN
     * @param timeout
     * @param amount
     * @param cardNo
     */
    PinInfo getPinpadOnlinePin(int timeout, String amount, String cardNo);

    /**
     *
     * @param timeout
     * @param i
     * @param key
     * @param offlinecounts
     * @return
     */
    PinInfo getPinpadOfflinePin(int timeout, int i, OfflineRSA key, int offlinecounts);

    /**
     * 人机交互显示UI接口(卡号确认)
     * @param cn 卡号
     */
    int showCardConfirm(int timeout, String cn);

    /**
     * 人机交互显示UI接口(多应用卡片选择)
     * @param timeout
     * @param list
     * @return
     */
    int showCardApplist(int timeout, String[] list);

    /**
     * 人机交互显示UI接口（多语言选择接口）
     * @param timeout
     * @param langs
     * @return
     */
    int showMultiLangs(int timeout, String[] langs);

    /**
     * 人机交互显示UI接口(耗时处理操作)
     * @param timeout
     * @param status TransStatus 状态标志以获取详细错误信息
     */
    void handling(int timeout, int status);

    /**
     * 人机交互显示UI接口
     * @param timeout
     * @param logData 详细交易日志
     */
    int showTransInfo(int timeout, TransLogData logData);

    /**
     * 交易成功处理结果
     * @param code
     */
    void trannSuccess(int timeout, int code, String... args);

    /**
     * 人机交互显示UI接口(显示交易出错错误信息)
     * @param errcode 实际代码错误返回码
     */
    void showError(int timeout, int errcode);
}

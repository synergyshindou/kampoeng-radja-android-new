package com.newpos.libpay.presenter;

import android.app.Activity;

import com.android.desert.keyboard.InputInfo;
import com.android.desert.keyboard.InputManager;
import com.newpos.libpay.Logger;
import com.newpos.libpay.PaySdk;
import com.newpos.libpay.PaySdkException;
import com.newpos.libpay.device.card.CardInfo;
import com.newpos.libpay.device.card.CardListener;
import com.newpos.libpay.device.card.CardManager;
import com.newpos.libpay.device.pinpad.OfflineRSA;
import com.newpos.libpay.device.pinpad.PinInfo;
import com.newpos.libpay.device.pinpad.PinpadListener;
import com.newpos.libpay.device.pinpad.PinpadManager;
import com.newpos.libpay.device.scanner.QRCInfo;
import com.newpos.libpay.device.scanner.QRCListener;
import com.newpos.libpay.device.scanner.ScannerManager;
import com.newpos.libpay.device.user.OnUserResultListener;
import com.newpos.libpay.global.TMConstants;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.PAYUtils;

import java.util.Locale;
import java.util.concurrent.CountDownLatch;

/**
 * Created by zhouqiang on 2017/4/25.
 * @author zhouqiang
 * 交易UI接口实现类
 * MVP架构中的P层 ，处理复杂的逻辑及数据
 */

public class TransUIImpl implements TransUI {

    private TransView transView = null ;
    private Activity mActivity = null ;

    public TransUIImpl(Activity activity , TransView tv){
        this.transView = tv ;
        this.mActivity = activity ;
    }

    private CountDownLatch mLatch ;
    private int mRet = 0 ;
    private InputManager.Style payStyle ;

    private int appNo = 0;

   /* final OnUserResultListener listener = new OnUserResultListener() {
        @Override
        public void confirm(InputManager.Style style) {
            mRet = 0 ;
            payStyle = style ;
            mLatch.countDown();
        }

        @Override
        public void cancel() {
            mRet = 1 ;
            mLatch.countDown();
        }

        //add by vincent
        public void setOtherInfo(int otherinfo){

        }
    };*/

    final IOnUserResultListener listener = new IOnUserResultListener();

    public class IOnUserResultListener implements OnUserResultListener{

        @Override
        public void confirm(InputManager.Style style) {
            mRet = 0 ;
            payStyle = style ;
            mLatch.countDown();
        }

        @Override
        public void cancel() {
            mRet = 1 ;
            mLatch.countDown();
        }

        //add by vincent
        public void setOtherInfo(int otherinfo){
            appNo = otherinfo;//app number
        }
    }


    @Override
    public PinInfo getPinpadOfflinePin(int timeout , int i , OfflineRSA key , int counts) {
        this.mLatch = new CountDownLatch(1);
        final PinInfo pinInfo = new PinInfo();
        PinpadManager pinpadManager = PinpadManager.getInstance();
        pinpadManager.getOfflinePin(i,key,counts,new PinpadListener() {
            @Override
            public void callback(PinInfo info) {
                pinInfo.setResultFlag(info.isResultFlag());
                pinInfo.setErrno(info.getErrno());
                pinInfo.setNoPin(info.isNoPin());
                pinInfo.setPinblock(info.getPinblock());
                mLatch.countDown();
            }
        });
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        transView.showMsgInfo(timeout , getStatusInfo(String.valueOf(Tcode.Status.handling)));
        return pinInfo ;
    }

    @Override
    public InputInfo getOutsideInput(int timeout, InputManager.Mode type) {
        transView.showInputView(timeout , type , listener);
        this.mLatch = new CountDownLatch(1);
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        InputInfo info = new InputInfo();
        if(mRet == 1){
            info.setResultFlag(false);
            info.setErrno(Tcode.T_user_cancel);
        }else {
            info.setResultFlag(true);
            info.setResult(transView.getInput(type));
            info.setNextStyle(payStyle);
        }
        return info;
    }

    @Override
    public CardInfo getCardUse(int timeout, int mode) {
        transView.showCardView(timeout , mode);
        this.mLatch = new CountDownLatch(1);
        final CardInfo cInfo = new CardInfo() ;
        CardManager cardManager = CardManager.getInstance(mode);
        cardManager.getCard(timeout, new CardListener() {
            @Override
            public void callback(CardInfo cardInfo) {
                cInfo.setResultFalg(cardInfo.isResultFalg());
                cInfo.setNfcType(cardInfo.getNfcType());
                cInfo.setCardType(cardInfo.getCardType());
                cInfo.setTrackNo(cardInfo.getTrackNo());
                cInfo.setCardAtr(cardInfo.getCardAtr());
                cInfo.setErrno(cardInfo.getErrno());
                mLatch.countDown();
            }
        });
        try {
            mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return cInfo;
    }

    @Override
    public QRCInfo getQRCInfo(final int timeout, final InputManager.Style mode) {
        transView.showQRCView(timeout , mode);
        this.mLatch = new CountDownLatch(1);
        final QRCInfo qinfo = new QRCInfo() ;
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ScannerManager manager = ScannerManager.getInstance(mActivity , mode);
                manager.getQRCode(timeout, new QRCListener() {
                    @Override
                    public void callback(QRCInfo info) {
                        qinfo.setResultFalg(info.isResultFalg());
                        qinfo.setErrno(info.getErrno());
                        qinfo.setQrc(info.getQrc());
                        mLatch.countDown();
                    }
                });
            }
        });

        try {
            mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return qinfo;
    }

    @Override
    public PinInfo getPinpadOnlinePin(int timeout, String amount, String cardNo) {
        this.mLatch = new CountDownLatch(1);
        final PinInfo pinInfo = new PinInfo();
        PinpadManager pinpadManager = PinpadManager.getInstance();
        pinpadManager.getPin(timeout,amount,cardNo, new PinpadListener() {
            @Override
            public void callback(PinInfo info) {
                pinInfo.setResultFlag(info.isResultFlag());
                pinInfo.setErrno(info.getErrno());
                pinInfo.setNoPin(info.isNoPin());
                pinInfo.setPinblock(info.getPinblock());
                mLatch.countDown();
            }
        });
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        transView.showMsgInfo(timeout , getStatusInfo(String.valueOf(Tcode.Status.handling)));
        return pinInfo ;
    }

    @Override
    public int showCardConfirm(int timeout, String cn) {
        this.mLatch = new CountDownLatch(1);
        transView.showCardNo(timeout, cn, listener);
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mRet;
    }

    //modify by vincent. delete (return mRet) instead of appNo 2017.11.21
    @Override
    public int showCardApplist(int timeout, String[] list) {
        Logger.debug("call back show app list>>>>>>>>>>>>>");
        this.mLatch = new CountDownLatch(1) ;
        transView.showCardAppListView(timeout, list, listener);
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return appNo;
    }

    @Override
    public int showMultiLangs(int timeout, String[] langs) {
        this.mLatch = new CountDownLatch(1);
        transView.showMultiLangView(timeout , langs , listener);
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mRet;
    }

    @Override
    public void handling(int timeout, int status) {
        transView.showMsgInfo(timeout , getStatusInfo(String.valueOf(status)));
    }

    @Override
    public int showTransInfo(int timeout, TransLogData logData) {
        this.mLatch = new CountDownLatch(1);
        transView.showTransInfoView(timeout, logData, listener);
        try {
            this.mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mRet;
    }

    @Override
    public void trannSuccess(int timeout , int code , String... args) {
        String info = getStatusInfo(String.valueOf(code)) ;
        if(args.length != 0){
            info += "\n"+args[0] ;
        }
        transView.showSuccess(timeout , info);
    }

    @Override
    public void showError(int timeout , int errcode) {
        transView.showError(timeout , getErrInfo(String.valueOf(errcode)));
    }

    /** ============================================= */

    private String getStatusInfo(String status){
        try {
            String[] infos = Locale.getDefault().getLanguage().equals("zh")?
                    PAYUtils.getProps(PaySdk.getInstance().getContext(), TMConstants.STATUS, status):
                    PAYUtils.getProps(PaySdk.getInstance().getContext(), TMConstants.STATUS_EN, status);
            if(infos!=null){
                return infos[0];
            }
        }catch (PaySdkException pse){
            pse.printStackTrace();
        }
        if(Locale.getDefault().getLanguage().equals("zh")){
            return "未知信息" ;
        }else {
            return "Unknown error" ;
        }
    }

    private String getErrInfo(String status){
        try {
            String[] errs = Locale.getDefault().getLanguage().equals("zh")?
                    PAYUtils.getProps(PaySdk.getInstance().getContext(), TMConstants.ERRNO, status):
                    PAYUtils.getProps(PaySdk.getInstance().getContext(), TMConstants.ERRNO_EN, status);
            if(errs!=null){
                return errs[0];
            }
        }catch (PaySdkException pse){
            pse.printStackTrace();
        }
        if(Locale.getDefault().getLanguage().equals("zh")){
            return "未知错误" ;
        }else {
            return "Unknown error" ;
        }
    }
}

package com.newpos.libpay.device.printer;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.SystemClock;

import com.newpos.libpay.Logger;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.presenter.TransUI;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.finace.FinanceTrans;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;
import com.pos.device.printer.PrintCanvas;
import com.pos.device.printer.PrintTask;
import com.pos.device.printer.Printer;
import com.pos.device.printer.PrinterCallback;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
/**
 * Created by zhouqiang on 2017/3/14.
 * @author zhouqiang
 * 打印管理类
 */
public class PrintManager {

	private static PrintManager mInstance ;
	private static TMConfig cfg ;

	private PrintManager(){}

	private static Context mContext;
	private static TransUI transUI;

	public static PrintManager getmInstance(Context c , TransUI tui){
		mContext = c ;
		transUI = tui ;
		if(null == mInstance){
			mInstance = new PrintManager();
		}
		cfg = TMConfig.getInstance();
		return mInstance ;
	}

	public static PrintManager getmInstance(Context c){
		mContext = c ;
		if(null == mInstance){
			mInstance = new PrintManager();
		}
		cfg = TMConfig.getInstance();
		return mInstance ;
	}

	private Printer printer = null ;
	private PrintTask printTask = null ;


	/**
	 * 打印签购单
	 * @param data
	 * @param isRePrint
     * @return
     */
	public int print(final TransLogData data, final boolean isRePrint){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		boolean isICC = data.isICC();
		boolean isNFC = data.isNFC();
		boolean isScan = data.isScan() ;
		int ret = -1;
		if (TransLog.getInstance().getSize() == 0) {
			ret = Tcode.T_print_no_log_err;
		}else {
			printer = Printer.getInstance() ;
			if (printer == null) {
				ret = Tcode.T_sdk_err ;
			}else{
				int num = cfg.getPrinterTickNumber() ;
				Logger.debug("PrintManager>>start>>PrinterTickNumber="+num);
				Bitmap image = PAYUtils.getLogoByBankId(mContext, cfg.getBankid());
				for (int i = 0; i < num; i++) {
					Logger.debug("PrintManager>>start>>for>>i="+i);
					PrintCanvas canvas = new PrintCanvas() ;
					Paint paint = new Paint() ;
					setFontStyle(paint , 2 , false);
					canvas.drawText(PrintRes.CH.WANNING , paint);
					printLine(paint , canvas);
					setFontStyle(paint , 1 , true);
					canvas.drawBitmap(image , paint);
					printLine(paint , canvas);
					setFontStyle(paint , 2 , false);
					if (i == 0) {
						canvas.drawText(PrintRes.CH.MERCHANT_COPY, paint);
					}else if (i == 1){
						canvas.drawText(PrintRes.CH.CARDHOLDER_COPY , paint);
					}else{
						canvas.drawText(PrintRes.CH.BANK_COPY , paint);
					}
					printLine(paint , canvas);
					setFontStyle(paint , 2 , false);
					canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
					canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
					canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
					String operNo = data.getOprNo() < 10 ? "0" + data.getOprNo() : data.getOprNo()+"";
					canvas.drawText(PrintRes.CH.OPERATOR_NO+"    "+operNo, paint);
					printLine(paint , canvas);
					setFontStyle(paint , 2 , false);
					canvas.drawText(PrintRes.CH.ISSUER, paint);
					canvas.drawText(PrintRes.CH.ACQUIRER, paint);
					if(isScan){
						canvas.drawText(PrintRes.CH.SCANCODE, paint);
					}else {
						canvas.drawText(PrintRes.CH.CARD_NO, paint);
					}
					setFontStyle(paint , 3 , true);
					if (isICC){
						canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " I" , paint);
					}else if(isNFC){
						canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " C" , paint);
					}else if(isScan){
						canvas.drawText("     "+ data.getPan() , paint);
					}else{
						canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " S" , paint);
					}
					setFontStyle(paint , 2 , false);
					canvas.drawText(PrintRes.CH.TRANS_TYPE , paint);
					setFontStyle(paint , 3 , true);
					canvas.drawText(formatTranstype(data.getEName()) , paint);
					setFontStyle(paint , 2 , false);
					if (!PAYUtils.isNullWithTrim(data.getExpDate())){
						canvas.drawText(PrintRes.CH.CARD_EXPDATE+"       " + data.getExpDate() , paint);
					}
					printLine(paint , canvas);
					setFontStyle(paint , 2 , false);
					if(!PAYUtils.isNullWithTrim(data.getBatchNo())){
						canvas.drawText(PrintRes.CH.BATCH_NO + data.getBatchNo(), paint);
					}
					if(!PAYUtils.isNullWithTrim(data.getTraceNo())){
						canvas.drawText(PrintRes.CH.VOUCHER_NO+data.getTraceNo(), paint);
					}
					if(!PAYUtils.isNullWithTrim(data.getAuthCode())){
						canvas.drawText(PrintRes.CH.AUTH_NO+data.getAuthCode() , paint);
					}
					setFontStyle(paint , 2 , false);
					if(!PAYUtils.isNullWithTrim(data.getLocalDate()) && !PAYUtils.isNullWithTrim(data.getLocalTime())){
						String timeStr = PAYUtils.StringPattern(data.getLocalDate() + data.getLocalTime(), "yyyyMMddHHmmss", "yyyy/MM/dd  HH:mm:ss");
						canvas.drawText(PrintRes.CH.DATE_TIME+"\n          " + timeStr, paint);
					}
					if(!PAYUtils.isNullWithTrim(data.getRRN())){
						canvas.drawText(PrintRes.CH.REF_NO+ data.getRRN(), paint);
					}
					canvas.drawText(PrintRes.CH.AMOUNT, paint);
					setFontStyle(paint , 3 , true);
					canvas.drawText("           "+ PrintRes.CH.RMB+"     "+ PAYUtils.getStrAmount(data.getAmount()) + "" , paint);
					printLine(paint , canvas);
					setFontStyle(paint , 1 , false);
					if(!PAYUtils.isNullWithTrim(data.getRefence())){
						canvas.drawText(PrintRes.CH.REFERENCE +"\n" + data.getRefence() , paint);
					}
					if(data.getICCData() != null){
						printAppendICCData(data.getICCData() , canvas , paint);
					}
					if (isRePrint) {
						setFontStyle(paint , 3 , true);
						canvas.drawText(PrintRes.CH.REPRINT , paint);
					}
					if (i != 1 && !isScan) {
						setFontStyle(paint , 3 , true);
						canvas.drawText("         "+ PrintRes.CH.CARDHOLDER_SIGN+"\n\n\n" , paint);
						printLine(paint , canvas);
						setFontStyle(paint , 1 , false);
						canvas.drawText(PrintRes.CH.AGREE_TRANS+"\n" , paint);
					}
					ret = printData(canvas);
				}
				if (printer != null) {
					printer = null;
				}
				if (image != null){
					image.recycle();
				}
			}
		}
		return ret;
	}

	/**
	 * 打印结算单
	 * @param data
	 * @return
     */
	public int printSettle(final TransLogData data){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret ;

		printer = Printer.getInstance() ;
		if(printer == null){
			return Tcode.T_sdk_err ;
		}

		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;

		//结算单
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , true);
		Bitmap image = PAYUtils.getLogoByBankId(mContext,cfg.getBankid());
		canvas.drawBitmap(image , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.SETTLE_SUMMARY, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
		canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
		canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
		String operNo = data.getOprNo() < 10 ? "0" + data.getOprNo() : data.getOprNo()+"";
		canvas.drawText(PrintRes.CH.OPERATOR_NO+"    "+operNo, paint);
		if(!PAYUtils.isNullWithTrim(data.getBatchNo())){
			canvas.drawText(PrintRes.CH.BATCH_NO + data.getBatchNo(), paint);
		}
		if(!PAYUtils.isNullWithTrim(data.getLocalDate()) && !PAYUtils.isNullWithTrim(data.getLocalTime())){
			String timeStr = PAYUtils.StringPattern(data.getLocalDate() + data.getLocalTime(), "yyyyMMddHHmmss", "yyyy/MM/dd  HH:mm:ss");
			canvas.drawText(PrintRes.CH.DATE_TIME+"\n          " + timeStr, paint);
		}
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_LIST , paint);
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_INNER_CARD , paint);
		List<TransLogData> list = TransLog.getInstance().getData();
		int saleAmount = 0 ;
		int saleSum = 0 ;
		int quickAmount = 0 ;
		int quickSum = 0 ;
		int voidAmount = 0 ;
		int voidSum = 0 ;
		for (int i = 0 ; i < list.size() ; i++ ){
			TransLogData tld = list.get(i) ;
			if(tld.getEName().equals(Trans.Type.SALE)){
				saleAmount += tld.getAmount() ;
				saleSum ++ ;
			}if(tld.getEName().equals(Trans.Type.QUICKPASS)){
				if(tld.getAAC() == FinanceTrans.AAC_ARQC){
					saleAmount += tld.getAmount() ;
					saleSum ++ ;
				}else{
					quickAmount += tld.getAmount() ;
					quickSum ++ ;
				}
			}if(tld.getEName().equals(Trans.Type.VOID)){
				voidAmount += tld.getAmount() ;
				voidSum ++ ;
			}
		}

		if(saleSum != 0){
			canvas.drawText(formatTranstype(Trans.Type.SALE)+"           "+saleSum+"               "+PAYUtils.getStrAmount(saleAmount) , paint);
		}if(quickSum != 0){
			canvas.drawText("电子现金消费/SALE"+"           "+quickSum+"               "+PAYUtils.getStrAmount(quickAmount) , paint);
		}if(voidSum != 0){
			canvas.drawText(formatTranstype(Trans.Type.VOID)+"           "+voidSum+"               "+PAYUtils.getStrAmount(voidAmount) , paint);
		}

		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_OUTER_CARD , paint);

		canvas.drawText("\n\n\n\n\n" , paint);

		//明细单
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , true);
		canvas.drawBitmap(image , paint);
		setFontStyle(paint , 2 , false);
		printLine(paint , canvas);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS_LIST_CH , paint);
		setFontStyle(paint , 1 , false);
		canvas.drawText(PrintRes.CH.SETTLE_DETAILS_LIST_EN , paint);
		setFontStyle(paint , 2 , false);
		printLine(paint , canvas);

		//添加明细
		List<TransLogData> list1 = TransLog.getInstance().getData();
		for (int i = 0 ; i < list1.size() ; i++ ){
			TransLogData tld = list1.get(i) ;
			if(tld.getEName().equals(Trans.Type.SALE) || tld.getEName().equals(Trans.Type.QUICKPASS) || tld.getEName().equals(Trans.Type.VOID)){
				canvas.drawText(tld.getTraceNo()+"     "+
						formatDetailsType(tld)+"    "+
						formatDetailsAuth(tld)+"    "+
						PAYUtils.getStrAmount(tld.getAmount())+"   "+
						tld.getPan() , paint);
			}
		}
		ret = printData(canvas);
		return ret ;
	}

	/**
	 * 打印交易明细
	 * @return
     */
	public int printDetails(){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		if (TransLog.getInstance().getSize() == 0) {
			ret = Tcode.T_print_no_log_err;
		}else {
			printer = Printer.getInstance() ;
			if (printer == null) {
				ret = Tcode.T_sdk_err ;
			}else{
				PrintCanvas canvas = new PrintCanvas() ;
				Paint paint = new Paint() ;
				setFontStyle(paint , 2 , true);
				canvas.drawText(PrintRes.CH.WANNING , paint);
				printLine(paint , canvas);
				setFontStyle(paint , 3 , true);
				canvas.drawText("                     "+ PrintRes.CH.DETAILS , paint);
				setFontStyle(paint , 2 , true);
				canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
				canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
				canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
				canvas.drawText(PrintRes.CH.BATCH_NO+"\n"+cfg.getBatchNo() , paint);
				canvas.drawText(PrintRes.CH.DATE_TIME+"\n"+PAYUtils.getSysTime(), paint);
				printLine(paint , canvas);
				int num = TransLog.getInstance().getSize() ;
				for (int i = 0 ; i < num ; i++){
					setFontStyle(paint , 1 , true);
					TransLogData data = TransLog.getInstance().get(i);
					if(data.isScan()){
						canvas.drawText(PrintRes.CH.SCANCODE+PAYUtils.getSecurityNum(data.getPan(), 6, 4), paint);
					}else {
						if(data.isICC()){
							canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " I", paint);
						}else if(data.isNFC()){
							canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " C", paint);
						}else {
							canvas.drawText(PrintRes.CH.CARD_NO+PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " S", paint);
						}
					}
					canvas.drawText(PrintRes.CH.TRANS_TYPE+formatTranstype(data.getEName()) , paint);
					canvas.drawText(PrintRes.CH.AMOUNT+ PrintRes.CH.RMB+PAYUtils.getStrAmount(data.getAmount()), paint);
					canvas.drawText(PrintRes.CH.VOUCHER_NO+data.getTraceNo(), paint);
					printLine(paint , canvas);
				}
				ret = printData(canvas);
				if (printer != null) {
					printer = null;
				}
			}
		}
		return ret;
	}

	public int printTest(){
		this.printTask = new PrintTask();
		this.printTask.setGray(130);
		int ret = -1;
		printer = Printer.getInstance() ;
		if (printer == null) {
			ret = Tcode.T_sdk_err ;
		}else{
			PrintCanvas canvas = new PrintCanvas() ;
			Paint paint = new Paint() ;
			setFontStyle(paint , 2 , true);
			canvas.drawText(PrintRes.CH.WANNING , paint);
			printLine(paint , canvas);
			setFontStyle(paint , 3 , true);
			canvas.drawText("                     "+ PrintRes.CH.DETAILS , paint);
			setFontStyle(paint , 2 , true);
			canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
			canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
			canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
			canvas.drawText(PrintRes.CH.BATCH_NO+"\n"+cfg.getBatchNo() , paint);
			canvas.drawText(PrintRes.CH.DATE_TIME+"\n"+PAYUtils.getSysTime(), paint);
			printLine(paint , canvas);
			ret = printData(canvas);
			if (printer != null) {
				printer = null;
			}
		}
		return ret;
	}

	/**
	 * 中信银行打印十笔测试
	 * @param data
	 * @param isRePrint
     * @return
     */
	int num  = 0 ;
	boolean isPrinting = false ;
	public int print10test(final TransLogData data , final boolean isRePrint){
		this.printTask = new PrintTask();
		final int ret ;
		boolean isICC = data.isICC();
		boolean isNFC = data.isNFC();
		boolean isScan = data.isScan() ;

		printer = Printer.getInstance() ;
		if(printer == null){
			return Tcode.T_sdk_err ;
		}

		PrintCanvas canvas = new PrintCanvas() ;
		Paint paint = new Paint() ;

		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.WANNING , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , true);
		Bitmap image = PAYUtils.getLogoByBankId(mContext,cfg.getBankid());
		canvas.drawBitmap(image , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.MERCHANT_COPY, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.MERCHANT_NAME+"\n"+cfg.getMerchName() , paint);
		canvas.drawText(PrintRes.CH.MERCHANT_ID+"\n"+cfg.getMerchID() , paint);
		canvas.drawText(PrintRes.CH.TERNIMAL_ID+"\n"+cfg.getTermID() , paint);
		String operNo = data.getOprNo() < 10 ? "0" + data.getOprNo() : data.getOprNo()+"";
		canvas.drawText(PrintRes.CH.OPERATOR_NO+"    "+operNo, paint);
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.ISSUER, paint);
		canvas.drawText(PrintRes.CH.ACQUIRER, paint);
		if(isScan){
			canvas.drawText(PrintRes.CH.SCANCODE, paint);
		}else {
			canvas.drawText(PrintRes.CH.CARD_NO, paint);
		}
		setFontStyle(paint , 3 , true);
		if (isICC){
			canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " I" , paint);
		}else if(isNFC){
			canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " C" , paint);
		}else if(isScan){
			canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) , paint);
		}else{
			canvas.drawText("     "+ PAYUtils.getSecurityNum(data.getPan(), 6, 4) + " S" , paint);
		}
		setFontStyle(paint , 2 , false);
		canvas.drawText(PrintRes.CH.TRANS_TYPE , paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText(formatTranstype(data.getEName()) , paint);
		setFontStyle(paint , 2 , false);
		if (!PAYUtils.isNullWithTrim(data.getExpDate())){
			canvas.drawText(PrintRes.CH.CARD_EXPDATE+"       " + data.getExpDate() , paint);
		}
		printLine(paint , canvas);
		setFontStyle(paint , 2 , false);
		if(!PAYUtils.isNullWithTrim(data.getBatchNo())){
			canvas.drawText(PrintRes.CH.BATCH_NO + data.getBatchNo(), paint);
		}
		if(!PAYUtils.isNullWithTrim(data.getTraceNo())){
			canvas.drawText(PrintRes.CH.VOUCHER_NO+data.getTraceNo(), paint);
		}
		if(!PAYUtils.isNullWithTrim(data.getAuthCode())){
			canvas.drawText(PrintRes.CH.AUTH_NO+data.getAuthCode() , paint);
		}
		setFontStyle(paint , 2 , false);
		if(!PAYUtils.isNullWithTrim(data.getLocalDate()) && !PAYUtils.isNullWithTrim(data.getLocalTime())){
			String timeStr = PAYUtils.StringPattern(data.getLocalDate() + data.getLocalTime(), "yyyyMMddHHmmss", "yyyy/MM/dd  HH:mm:ss");
			canvas.drawText(PrintRes.CH.DATE_TIME+"\n          " + timeStr, paint);
		}
		if(!PAYUtils.isNullWithTrim(data.getRRN())){
			canvas.drawText(PrintRes.CH.REF_NO+ data.getRRN(), paint);
		}
		canvas.drawText(PrintRes.CH.AMOUNT, paint);
		setFontStyle(paint , 3 , true);
		canvas.drawText("           "+ PrintRes.CH.RMB+"     "+ PAYUtils.getStrAmount(data.getAmount()) + "" , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , false);
		if(!PAYUtils.isNullWithTrim(data.getRefence())){
			canvas.drawText(PrintRes.CH.REFERENCE +"\n" + data.getRefence() , paint);
		}
		//追加ICC数据
		if ( data.getICCData() != null ) {
			printAppendICCData(data.getICCData() , canvas , paint);
		}
		if (isRePrint) {
			setFontStyle(paint , 3 , true);
			canvas.drawText(PrintRes.CH.REPRINT , paint);
		}
		setFontStyle(paint , 3 , true);
		canvas.drawText("       "+ PrintRes.CH.CARDHOLDER_SIGN+"\n\n\n" , paint);
		printLine(paint , canvas);
		setFontStyle(paint , 1 , false);
		canvas.drawText(PrintRes.CH.AGREE_TRANS+"\n" , paint);

		printTask.setPrintCanvas(canvas);
		ret = printer.getStatus();
		isPrinting = false ;
		num = 0 ;
		Logger.debug("printer.getStatus="+ret);
		do {
			if(!isPrinting){
				isPrinting = true ;
				printer.startPrint(printTask, new PrinterCallback() {
					@Override
					public void onResult(int i, PrintTask printTask) {
						Logger.debug("PrinterCallback i = " + i);
						isPrinting = false ;
						num++ ;
					}
				});
			}
		}while (num < 10);
		return ret ;
	}

	/**
	 * 调用驱动打印
	 * @param pCanvas
	 * @return
     */
	private int printData(PrintCanvas pCanvas) {
		final CountDownLatch latch = new CountDownLatch(1);
		printer = Printer.getInstance() ;
		int ret = printer.getStatus();
		Logger.debug("打印机状态："+ret);
		if(Printer.PRINTER_STATUS_PAPER_LACK == ret){
			Logger.debug("打印机缺纸，提示用户装纸");
			if (transUI != null)
				transUI.handling(60*1000 , Tcode.Status.printer_lack_paper);
			long start = SystemClock.uptimeMillis() ;
			while (true){
				if(SystemClock.uptimeMillis() - start > 60 * 1000){
					ret = Printer.PRINTER_STATUS_PAPER_LACK ;
					break;
				}
				if(printer.getStatus() == Printer.PRINTER_OK){
					ret = Printer.PRINTER_OK ;
					break;
				}else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						Logger.debug("printer task interrupted");
					}
				}
			}
		}
		Logger.debug("开始打印");
		if(ret == Printer.PRINTER_OK){
			if (transUI != null)
				transUI.handling(60*1000 , Tcode.Status.printing_recept);
			printTask.setPrintCanvas(pCanvas);
			printer.startPrint( printTask , new PrinterCallback() {
				@Override
				public void onResult(int i, PrintTask printTask) {
					latch.countDown();
				}
			});
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return ret ;
	}

	/**
	 * 检查打印机状态
	 * @return
     */
	private int checkPrinterStatus() {
		long t0 = System.currentTimeMillis();
		int ret ;
		while (true) {
			if (System.currentTimeMillis() - t0 > 30000) {
				ret = -1 ;
				break;
			}
			ret = printer.getStatus();
			Logger.debug("printer.getStatus() ret = "+ret);
			if (ret == Printer.PRINTER_OK) {
				Logger.debug("printer.getStatus()=Printer.PRINTER_OK");
				Logger.debug("打印机状态正常");
				break;
			}else if (ret == -3) {
				Logger.debug("printer.getStatus()=Printer.PRINTER_STATUS_PAPER_LACK");
				Logger.debug("提示用户装纸...");
				break;
			} else if (ret == Printer.PRINTER_STATUS_BUSY) {
				Logger.debug("printer.getStatus()=Printer.PRINTER_STATUS_BUSY");
				Logger.debug("打印机忙");
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				break;
			}
		}
		return ret;
	}

	/** =======================私有处理方法=====================**/

	private String formatTranstype(String type){
		int index = 0 ;
		for (int i = 0; i < PrintRes.TRANSEN.length ; i++){
			if(PrintRes.TRANSEN[i].equals(type)){
				index = i ;
			}
		}
		if(Locale.getDefault().getLanguage().equals("zh")){
			return PrintRes.TRANSCH[index]+"("+type+")";
		}else {
			return type ;
		}
	}

	private String formatDetailsType(TransLogData data){
		if(data.isICC()){
			return "I" ;
		}else if(data.isNFC()){
			return "C" ;
		}else {
			return "S" ;
		}
	}

	private String formatDetailsAuth(TransLogData data){
		if(data.getAuthCode() == null){
			return "000000" ;
		}else {
			return data.getAuthCode() ;
		}
	}

	/**
	 * 设置打印字体样式
	 * @param paint 画笔
	 * @param size 字体大小 1---small , 2---middle , 3---large
	 * @param isBold 是否加粗
	 * @author zq
	 */
	private void setFontStyle(Paint paint , int size , boolean isBold){
		if(isBold) {
			paint.setTypeface(Typeface.DEFAULT_BOLD);
		}else {
			paint.setTypeface(Typeface.DEFAULT);
		}
		switch (size) {
			case 0 : break;
			case 1 : paint.setTextSize(16F) ;break;
			case 2 : paint.setTextSize(22F) ;break;
			case 3 : paint.setTextSize(30F) ;break;
			default:break;
		}
	}

	/**
	 * 在画布上画出一条线
	 * @param paint
	 * @param canvas
	 */
	private void printLine(Paint paint , PrintCanvas canvas){
		String line = "----------------------------------------------------------------";
		setFontStyle(paint , 1 , true);
		canvas.drawText(line , paint);
	}

	private void printAppendICCData(byte[] ICCData , PrintCanvas canvas , Paint paint){
		byte[] temp = new byte[256];
		int len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x4F, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AID: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x50, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("LABLE: "+ ISOUtil.hex2AsciiStr(ISOUtil.byte2hex(temp, 0, len)) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F26, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TC: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x5F34,temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("PanSN: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x95, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TVR: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9B, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TSI: "+ ISOUtil.byte2hex(temp, 0, len) , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F36, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("ATC: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F33, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("TermCap: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F09, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AppVer: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F34, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("CVM: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F10, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("IAD: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x82, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("AIP: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
		len = PAYUtils.get_tlv_data(ICCData, ICCData.length, 0x9F1E, temp, false);
		if (!PAYUtils.isNullWithTrim(ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len)))){
			canvas.drawText("IFD: "+ ISOUtil.byte2hex(temp, 0, len) + "" , paint);
		}
	}
}

package com.newpos.libpay.trans.finace;

import android.content.Context;

import com.newpos.libpay.Logger;
import com.newpos.libpay.device.pinpad.PinpadManager;
import com.newpos.libpay.device.printer.PrintManager;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.trans.Tcode;
import com.newpos.libpay.trans.Trans;
import com.newpos.libpay.trans.manager.RevesalTrans;
import com.newpos.libpay.trans.manager.ScriptTrans;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.trans.translog.TransLogData;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;
import com.pos.device.printer.Printer;

/**
 * 金融交易类
 * @author zhouqiang
 */
public class FinanceTrans extends Trans {

	/**
	 * 外界输入类型
	 */
	public static final int INMODE_HAND = 0x01;
	public static final int INMODE_MAG = 0x02;
	public static final int INMODE_QR = 0x04;
	public static final int INMODE_IC = 0x08;
	public static final int INMODE_NFC = 0x10;

	/**
	 * 联机交易还是脱机交易
	 */
	public static final int AAC_ARQC = 1 ;
	public static final int AAC_TC = 0 ;

    /** 卡片模式 */
	protected int inputMode = 0x02;// 刷卡模式 1 手输卡号；2刷卡；5 3插IC；7 4非接触卡

	/**
	 * 是否有密码
	 */
	protected boolean isPinExist = false;

	/**
	 * 是否式IC卡
	 */
	protected boolean isICC = false;

	/**
	 * 标记此次交易是否需要冲正
	 */
	protected boolean isReversal;

	/**
	 * 标记此次交易是否需要存记录
	 */
	protected boolean isSaveLog;

	/**
	 * 是否借记卡交易
	 */
	protected boolean isDebit;

	/**
	 * 标记此交易联机前是否进行冲正上送
	 */
	protected boolean isProcPreTrans;

	/**
	 * 后置交易
	 */
	protected boolean isProcSuffix;

	/**
	 * 金融交易类构造
	 * @param ctx
	 * @param transEname
     */
	public FinanceTrans(Context ctx, String transEname) {
		super(ctx, transEname);
		iso8583.setHasMac(true);
		setTraceNoInc(true);
	}

	/**
	 * 联机前某些特殊值的处理
	 * @param inputMode
     */
	protected void setDatas(int inputMode) {
		Logger.debug("==FinanceTrans->setDatas==");
		this.inputMode = inputMode;
		if (isPinExist) {
			CaptureCode = "12";
		}
		EntryMode = ISOUtil.padleft(inputMode + "", 2, '0');
		if (isPinExist){
			EntryMode += "10";
		}else{
			EntryMode += "20";
		}
		if (isPinExist || Track2 != null || Track3 != null) {
			if(isPinExist) {
				SecurityInfo = "2";
			} else {
				SecurityInfo = "0";
			}
			if (cfg.isSingleKey()) {
				SecurityInfo += "0";
			} else {
				SecurityInfo += "6";
			}
			if (cfg.isTrackEncrypt()) {
				SecurityInfo += "10000000000000";
			} else {
				SecurityInfo += "00000000000000";
			}
		}
		appendField60("048");
	}

	/**
	 * 从内核获取
	 * 卡号，
	 * 有效期，
	 * 2磁道，
	 * 1磁道，
	 * 卡序号
	 * 55域数据
	 */
	protected void setICCData(){
		Logger.debug("==FinanceTrans->setICCData==");
		byte[] temp = new byte[128];
		// 卡号
		int len = PAYUtils.get_tlv_data_kernal(0x5A, temp);
		Pan = ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len));
		// 有效期
		len = PAYUtils.get_tlv_data_kernal(0x5F24, temp);
		if(len==3) {
			ExpDate = ISOUtil.byte2hex(temp, 0, len - 1);
		}
		// 2磁道
		len = PAYUtils.get_tlv_data_kernal(0x57, temp);
		Track2 = ISOUtil.trimf(ISOUtil.byte2hex(temp, 0, len));
		// 1磁道
		len = PAYUtils.get_tlv_data_kernal(0x9F1F, temp);
		Track1 = new String(temp, 0, len);
		// 卡序号
		len = PAYUtils.get_tlv_data_kernal(0x5F34, temp);
		PanSeqNo = ISOUtil.padleft(ISOUtil.byte2int(temp, 0, len) + "", 3, '0');
		//55域数据
		temp = new byte[512];
		len = PAYUtils.pack_tags(PAYUtils.wOnlineTags, temp);
		if (len > 0) {
			ICCData = new byte[len];
			System.arraycopy(temp, 0, ICCData, 0, len);
		} else{
			ICCData = null;
		}
	}

	/**
	 * 设置交易报文8583各域值，设置完后判断冲正等，即可联机
	 */
	protected void setFields() {
        Logger.debug("==FinanceTrans->setFields==");
		int[] trackLen = new int[2];
		byte[] encryTrack = new byte[256];
		if (MsgID != null) {
			iso8583.setField(0, MsgID);
		}
		if (Pan != null) {
			iso8583.setField(2, Pan);
		}
		if (ProcCode != null) {
			iso8583.setField(3, ProcCode);
		}
		if (Amount > 0) {
			String AmoutData = "";
			AmoutData = ISOUtil.padleft(Amount + "", 12, '0');
			iso8583.setField(4, AmoutData);
		}
		if (TraceNo != null) {
			iso8583.setField(11, TraceNo);
		}
		if (LocalTime != null) {
			iso8583.setField(12, LocalTime);
		}
		if (LocalDate != null) {
			iso8583.setField(13, LocalDate);
		}
		if (ExpDate != null) {
			iso8583.setField(14, ExpDate);
		}
		if (SettleDate != null) {
			iso8583.setField(15, SettleDate);
		}
		if (EntryMode != null) {
			iso8583.setField(22, EntryMode);
		}
		if (PanSeqNo != null) {
			iso8583.setField(23, PanSeqNo);
		}
		if (SvrCode != null) {
			iso8583.setField(25, SvrCode);
		}
		if (CaptureCode != null) {
			iso8583.setField(26, CaptureCode);
		}
		if (AcquirerID != null) {
			iso8583.setField(32, AcquirerID);
		}
		if (Track2 != null && cfg.isTrackEncrypt()) {
            Track2 = PinpadManager.getInstance().getEac(0 , Track2);
		}
		iso8583.setField(35, Track2);
		if (Track3 != null && cfg.isTrackEncrypt()) {
            Track3 = PinpadManager.getInstance().getEac(0 , Track3 );
		}
		iso8583.setField(36, Track3);
		if (RRN != null) {
			iso8583.setField(37, RRN);
		}
		if (AuthCode != null) {
			iso8583.setField(38, AuthCode);
		}
		if (RspCode != null) {
			iso8583.setField(39, RspCode);
		}
		if (TermID != null) {
			iso8583.setField(41, TermID);
		}
		if (MerchID != null) {
			iso8583.setField(42, MerchID);
		}
		if (Field44 != null) {
			iso8583.setField(44, Field44);
		}
		if (Field48 != null) {
			iso8583.setField(48, Field48);
		}
		if (CurrencyCode != null) {
			iso8583.setField(49, CurrencyCode);
		}
		if (PIN != null) {
			iso8583.setField(52, PIN);
		}
		if (SecurityInfo != null) {
			iso8583.setField(53, SecurityInfo);
		}
		if (ExtAmount != null) {
			iso8583.setField(54, ExtAmount);
		}
		if (ICCData != null) {
			iso8583.setField(55, ISOUtil.byte2hex(ICCData));
		}
		if (Field60 != null) {
			iso8583.setField(60, Field60);
		}
		if (Field61 != null) {
			iso8583.setField(61, Field61);
		}
		if (Field62 != null) {
			iso8583.setField(62, Field62);
		}
		if (Field63 != null) {
			iso8583.setField(63, Field63);
		}
	}

	/**
	 * 联机处理
	 * @param emvTrans
     * @return
     */
	protected int OnlineTrans(EmvTransaction emvTrans) {
		if(!cfg.isOnline()){
			return LocalPresentations();
		}

		byte[] tag9f27 = new byte[1];
		byte[] tag9b = new byte[2];
		setFields();
        if (isProcPreTrans) {
            TransLogData revesalData = TransLog.getReversal();
            if (revesalData != null) {
				transUI.handling(timeout , Tcode.Status.terminal_reversal);
                RevesalTrans revesal = new RevesalTrans(context, "REVERSAL");
                for (int i = 0; i < cfg.getReversalCount() ; i++) {
                    retVal = revesal.sendRevesal();
					if(retVal == 0){
						//冲正成功，清除冲正
						TransLog.clearReveral();
						break;
					}else {
						if(retVal != Tcode.T_socket_err && retVal != Tcode.T_send_err){
							continue;
						}
					}
                }
				if(retVal == Tcode.T_socket_err || retVal == Tcode.T_send_err){
					//网络错误不能清除冲正，直接返回
					return retVal ;
				}else {
					if(retVal != 0){
						//冲正失败，清除冲正，结束交易
						TransLog.clearReveral();
						return Tcode.T_reversal_fail ;
					}
				}
            }
        }

		transUI.handling(timeout , Tcode.Status.connecting_center);
        if (connect() == -1){
			return Tcode.T_socket_err ;
		}

		if (isReversal) {
			Logger.debug("FinanceTrans->OnlineTrans->save Reversal");
			TransLogData Reveral = setReveralData();
			TransLog.saveReversal(Reveral);
		}

		transUI.handling(timeout , Tcode.Status.send_data_2_server);
		retVal = send();
		if (retVal == -1){
			return Tcode.T_send_err ;
		}

		if(retVal == 0){
			if (isTraceNoInc) {
				cfg.incTraceNo();
			}
		}

		transUI.handling(timeout , Tcode.Status.send_over_2_recv);
		byte[] respData = recive();
		netWork.close();
		if (respData == null){
			return Tcode.T_receive_err ;
		}

		retVal = iso8583.unPacketISO8583(respData);
		if(retVal!=0){
			Logger.debug("unpacket fail,check mac please");
			if(retVal == Tcode.T_package_mac_err){
				if(isReversal){
					//返回报文校验MAC错误，更新冲正原因A0
					TransLogData newR = TransLog.getReversal() ;
					newR.setRspCode("A0");
					TransLog.clearReveral();
					TransLog.saveReversal(newR);
				}
			}
			return retVal ;
		}

		RspCode = iso8583.getfield(39);
		AuthCode = iso8583.getfield(38);
		String strICC = iso8583.getfield(55);
		if (strICC != null && (!strICC.trim().equals(""))){
			ICCData = ISOUtil.str2bcd(strICC, false);
		}else{
			ICCData = null;
		}
		if(!"00".equals(RspCode)){
			TransLog.clearReveral();
			return formatRsp(RspCode);
		}
		boolean need2AC = TransEName.equals(Type.SALE) || TransEName.equals(Type.QUICKPASS) ;
		if (emvTrans != null && retVal == 0 && need2AC ){
			retVal = emvTrans.afterOnline(RspCode, AuthCode, ICCData, retVal);
			int lenOf9f27 = PAYUtils.get_tlv_data_kernal(0x9F27, tag9f27);
			if (lenOf9f27 != 1) {
				// IC失败处理 如果39域是00 更新冲正文件 39域 06
				TransLogData revesalData = TransLog.getReversal();
				if(revesalData!=null){
					revesalData.setRspCode("06");
					TransLog.saveReversal(revesalData);
				}
			}
			if (tag9f27[0] != 0x40) {// 后台批准，被卡片拒绝，要保留冲正
				return Tcode.T_gen_2_ac_fail ;
			}
			//发卡行脚本结果
			int len9b = PAYUtils.get_tlv_data_kernal(0x9b, tag9b);
			if (len9b == 2 && (tag9b[0] & 0x04) != 0) {//脚本已执行
				// 存发卡行脚本结果
				byte[] temp = new byte[256];
				int len = PAYUtils.pack_tags(PAYUtils.wISR_tags, temp);
				if (len > 0) {
					ICCData = new byte[len];
					System.arraycopy(temp, 0, ICCData, 0, len);
				} else{
					ICCData = null;
				}
				TransLogData scriptResult = setScriptData();
				TransLog.saveScriptResult(scriptResult);
			}
		}

		if (retVal != 0){
			return retVal ;
		}

		//脚本上送
		TransLogData data = TransLog.getScriptResult();
		if (data != null) {
			ScriptTrans script = new ScriptTrans(context, "SENDSCRIPT");
			int ret = script.sendScriptResult(data);
			if (ret == 0) {
				TransLog.clearScriptResult();
			}
		}

		// 存交易记录
		if (isSaveLog) {
			TransLogData logData = setLogData();
			transLog.saveLog(logData);
		}
		TransLog.clearReveral();

		if(para.isNeedPrint()){
			transUI.handling(timeout , Tcode.Status.printing_recept);
			PrintManager printManager = PrintManager.getmInstance(context , transUI);
			do{
				retVal = printManager.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				retVal = 0 ;
			} else {
				retVal = Tcode.T_printer_exception;
			}
		}

		return retVal ;
	}

	/**
	 * 设置从服务器传过来的交易信息 Data From server
	 * @return TransLog
	 */
	private TransLogData setLogData() {
		TransLogData LogData = new TransLogData();
		LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
		LogData.setOprNo(cfg.getOprNo());
		LogData.setBatchNo(BatchNo);
		LogData.setEName(TransEName);
		LogData.setAAC(FinanceTrans.AAC_ARQC);
        LogData.setTraceNo(iso8583.getfield(11));
        LogData.setLocalTime(iso8583.getfield(12));
        LogData.setLocalDate(PAYUtils.getYear() + iso8583.getfield(13));
        LogData.setExpDate(iso8583.getfield(14));
        LogData.setSettleDate(iso8583.getfield(15));
        LogData.setEntryMode(iso8583.getfield(22));
        LogData.setPanSeqNo(iso8583.getfield(23));
        LogData.setAcquirerID(iso8583.getfield(32));
        LogData.setRRN(iso8583.getfield(37));
        LogData.setAuthCode(iso8583.getfield(38));
        LogData.setRspCode(iso8583.getfield(39));
        LogData.setField44(iso8583.getfield(44));
        LogData.setCurrencyCode(iso8583.getfield(49));
		LogData.setICCData(ICCData);
		if(inputMode == ENTRY_MODE_NFC){
			LogData.setNFC(true);
		}if(inputMode == ENTRY_MODE_ICC) {
			LogData.setICC(true);
		}
		if(TransEName.equals(Type.ENQUIRY)){
			String f54 = iso8583.getfield(54) ;
			LogData.setAmount(Long.parseLong(f54.substring(f54.indexOf('C')+1 , f54.length())));
		}else{
			LogData.setAmount(Long.parseLong(iso8583.getfield(4)));
			String field63 = iso8583.getfield(63);
			String IssuerName = field63.substring(0, 3);
			String ref = field63.substring(3, field63.length());
			LogData.setRefence(ref);
			LogData.setIssuerName(IssuerName);
		}
		return LogData;
	}

	/**
	 * 保存扫码交易数据
	 * @param code 付款码
	 * @return
     */
	protected TransLogData setScanData(String code){
		TransLogData LogData = new TransLogData();
		LogData.setAmount(Amount);
		LogData.setPan(code);
		LogData.setOprNo(cfg.getOprNo());
		LogData.setBatchNo(BatchNo);
		LogData.setEName(TransEName);
		LogData.setICCData(ICCData);
		if(inputMode == ENTRY_MODE_NFC){
			LogData.setNFC(true);
		}if(inputMode == ENTRY_MODE_ICC) {
			LogData.setICC(true);
		}if(inputMode == ENTRY_MODE_QRC){
			LogData.setScan(true);
		}
		LogData.setLocalDate(PAYUtils.getYMD());
		LogData.setTraceNo(TraceNo);
		LogData.setLocalTime(PAYUtils.getHMS());
		LogData.setSettleDate(PAYUtils.getYMD());
		LogData.setAcquirerID("12345678");
		LogData.setRRN("170907084952");
		LogData.setAuthCode("084952");
		LogData.setRspCode("00");
		LogData.setField44("0425       0461       ");
		LogData.setCurrencyCode("156");
		return LogData;
	}

	/**
	 * 脱机打单
	 * @param ec_amount
     * @return
     */
	protected int offlineTrans(String ec_amount){
		if (isSaveLog) {
			TransLogData LogData = new TransLogData();
			if(para.getTransType().equals(Type.EC_ENQUIRY)){
				LogData.setAmount(Long.parseLong(ec_amount));
			}else {
				LogData.setAmount(Amount);
			}
			LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
			LogData.setOprNo(cfg.getOprNo());
			LogData.setEName(TransEName);
			LogData.setEntryMode(ISOUtil.padleft(inputMode + "", 2, '0')+"10");
			LogData.setTraceNo(cfg.getTraceNo());
			LogData.setBatchNo(cfg.getBatchNo());
			LogData.setLocalDate(PAYUtils.getYear() + PAYUtils.getLocalDate());
			LogData.setLocalTime(PAYUtils.getLocalTime());
			LogData.setAAC(FinanceTrans.AAC_TC);
			LogData.setICCData(ICCData);
			if(inputMode == ENTRY_MODE_NFC){
				LogData.setNFC(true);
			}if(inputMode == ENTRY_MODE_ICC) {
				LogData.setICC(true);
			}
			transLog.saveLog(LogData);
			if(isTraceNoInc){
				cfg.incTraceNo();
			}
		}
		if(para.isNeedPrint()){
			transUI.handling(timeout , Tcode.Status.printing_recept);
			PrintManager print = PrintManager.getmInstance(context , transUI);
			do{
				retVal = print.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				return 0 ;
			}else {
				return Tcode.T_printer_exception ;
			}
		}else {
			return 0 ;
		}
	}

	/**
	 * 设置发卡行脚本数据
	 * @return
     */
	private TransLogData setScriptData() {
		TransLogData LogData = new TransLogData();
		LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
		LogData.setICCData(ICCData);
		LogData.setBatchNo(BatchNo);
        LogData.setAmount(Long.parseLong(iso8583.getfield(4)));
        LogData.setTraceNo(iso8583.getfield(11));
        LogData.setLocalTime(iso8583.getfield(12));
        LogData.setLocalDate(iso8583.getfield(13));
        LogData.setEntryMode(iso8583.getfield(22));
        LogData.setPanSeqNo(iso8583.getfield(23));
        LogData.setAcquirerID(iso8583.getfield(32));
        LogData.setRRN(iso8583.getfield(37));
        LogData.setAuthCode(iso8583.getfield(38));
        LogData.setCurrencyCode(iso8583.getfield(49));
		return LogData ;
	}

	/**
	 * 设置冲正数据
	 * @return
     */
	private TransLogData setReveralData() {
		TransLogData LogData = new TransLogData();
		LogData.setPan(Pan);
		LogData.setProcCode(ProcCode);
		LogData.setAmount(Amount);
		LogData.setTraceNo(TraceNo);
		LogData.setExpDate(ExpDate);
		LogData.setEntryMode(EntryMode);
		LogData.setPanSeqNo(PanSeqNo);
		LogData.setSvrCode(SvrCode);
		LogData.setAuthCode(AuthCode);
		LogData.setRspCode("98");
		LogData.setCurrencyCode(CurrencyCode);
        byte[] temp = new byte[156];
        if (inputMode == ENTRY_MODE_ICC || inputMode == ENTRY_MODE_NFC) {
            int len = PAYUtils.pack_tags(PAYUtils.reversal_tag, temp);
            if (len > 0) {
                ICCData = new byte[len];
                System.arraycopy(temp, len, ICCData, 0, len);
                LogData.setICCData(ICCData);
            } else {
				ICCData = null;
			}
        }
		LogData.setField60(Field60);
		return LogData;
	}

	/**
	 * 格式化处理响应码
	 * @param rsp
	 * @return
     */
	private int formatRsp(String rsp){
		String[] stand_rsp = {"5A","5B","6A","A0","D1","D2","D3","D4","N6","N7"} ;
		int START = 200 ;
		boolean finded = false ;
		for (int i = 0 ; i < stand_rsp.length ; i++){
			if(stand_rsp[i].equals(rsp)){
				START += i ;
				finded = true ;
				break;
			}
		}
		if(finded){
			return START ;
		}else {
			return Integer.parseInt(rsp) ;
		}
	}

	/**
	 * 不进行报文联机，用于无网演示
	 * 将不对数据进行任何校验与处理
	 * @return
     */
	private int LocalPresentations(){
		if (isSaveLog) {
			TransLogData LogData = new TransLogData();
			LogData.setAmount(Amount);
			LogData.setPan(PAYUtils.getSecurityNum(Pan, 6, 4));
			LogData.setOprNo(cfg.getOprNo());
			LogData.setEName(TransEName);
			LogData.setEntryMode(ISOUtil.padleft(inputMode + "", 2, '0')+"10");
			LogData.setTraceNo(cfg.getTraceNo());
			LogData.setBatchNo(cfg.getBatchNo());
			LogData.setLocalDate(PAYUtils.getYear() + PAYUtils.getLocalDate());
			LogData.setLocalTime(PAYUtils.getLocalTime());
			LogData.setAuthCode(PAYUtils.getLocalTime());
			LogData.setAAC(FinanceTrans.AAC_TC);
			LogData.setICCData(ICCData);
			if(inputMode == ENTRY_MODE_NFC){
				LogData.setNFC(true);
			}if(inputMode == ENTRY_MODE_ICC) {
				LogData.setICC(true);
			}
			transLog.saveLog(LogData);
			if(isTraceNoInc){
				cfg.incTraceNo();
			}
		}
		if(para.isNeedPrint()){
			Logger.debug("FinanceTrans>>NotNeedOnline>>开始打单");
			transUI.handling(timeout , Tcode.Status.printing_recept);
			PrintManager print = PrintManager.getmInstance(context , transUI);
			do{
				retVal = print.print(transLog.getLastTransLog(), false);
			}while (retVal == Printer.PRINTER_STATUS_PAPER_LACK);
			if (retVal == Printer.PRINTER_OK) {
				return 0 ;
			}else {
				return Tcode.T_printer_exception ;
			}
		}else {
			return 0 ;
		}
	}
}

package com.newpos.libpay.trans;

import android.content.Context;

import com.newpos.libpay.Logger;
import com.newpos.libpay.global.TMConfig;
import com.newpos.libpay.global.TMConstants;
import com.newpos.libpay.helper.iso8583.ISO8583;
import com.newpos.libpay.helper.ssl.NetworkHelper;
import com.newpos.libpay.presenter.TransUI;
import com.newpos.libpay.process.EmvTransaction;
import com.newpos.libpay.process.QpbocTransaction;
import com.newpos.libpay.trans.translog.TransLog;
import com.newpos.libpay.utils.ISOUtil;
import com.newpos.libpay.utils.PAYUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * 交易抽象类，定义所有交易类的父类
 * @author zhouqiang
 */
public abstract class Trans {

	/**
	 * 上下文对象
	 */
	protected Context context;

	/**
	 * 8583组包解包
	 */
	protected ISO8583 iso8583;

	/**
	 * 网络操作对象
	 */
	protected NetworkHelper netWork;

	/**
	 * 交易记录的集合
	 */
	protected TransLog transLog;

	/**
	 * 配置文件操作实例
	 */
	protected TMConfig cfg;

	/**
	 * MODEL与VIEW层接口实例
	 */
	protected TransUI transUI ;

	/**
	 * 等待页面超时时间
	 */
	protected int timeout ;

	/**
	 * 返回值全局定义
	 */
	protected int retVal ;

	/**
	 * EMV流程控制实例
	 */
	protected EmvTransaction emv ;

	/**
	 * QPBOC流程控制实例
	 */
	protected QpbocTransaction qpboc ;

	/**
	 * 交易相关参数集合
	 */
	protected TransInputPara para ;

	/**
	 * 交易类型定义
	 */
	public interface Type{
		public static final String LOGON = "LOGON" ;
		public static final String DOWNPARA = "DOWNPARA" ;
		public static final String QUERY_EMV_CAPK = "QUERY_EMV_CAPK" ;
		public static final String DOWNLOAD_EMV_CAPK = "DOWNLOAD_EMV_CAPK" ;
		public static final String DOWNLOAD_EMV_CAPK_END = "DOWNLOAD_EMV_CAPK_END" ;
		public static final String QUERY_EMV_PARAM = "QUERY_EMV_PARAM" ;
		public static final String DOWNLOAD_EMV_PARAM = "DOWNLOAD_EMV_PARAM" ;
		public static final String DOWNLOAD_EMV_PARAM_END = "DOWNLOAD_EMV_PARAM_END" ;
		public static final String LOGOUT = "LOGOUT" ;
		public static final String SALE = "SALE" ;
		public static final String ENQUIRY = "ENQUIRY" ;
		public static final String VOID = "VOID" ;
		public static final String EC_ENQUIRY = "EC_ENQUIRY" ;
		public static final String QUICKPASS = "QUICKPASS" ;
		public static final String REFUND = "REFUND" ;
		public static final String TRANSFER = "TRANSFER" ;
		public static final String CREFORLOAD = "CREFORLOAD" ;
		public static final String DEBFORLOAD = "DEBFORLOAD" ;
		public static final String SETTLE = "SETTLE" ;
		public static final String UPSEND = "UPSEND" ;
		public static final String REVERSAL = "REVERSAL" ;
		public static final String SENDSCRIPT = "SENDSCRIPT" ;
		public static final String SCANSALE = "SCANSALE" ;
		public static final String SCANVOID = "SCANVOID" ;
		public static final String SCANREFUND = "SCANREFUND" ;
	}

	/** 报文域定义 */

	/**
	 * 0 消息类型
	 */
	protected String MsgID;

	/**
	 * 2* 卡号
	 */
	protected String Pan;

	/**
	 *  3  预处理码
	 */
	protected String ProcCode;

	/**
	 * 4* 金额
	 */
	protected long Amount;

	/**
	 * 11域交易流水号
	 */
	protected String TraceNo;

	/**
	 * 12 hhmmss*
	 */
	protected String LocalTime;

	/**
	 * 13 MMDD*
	 */
	protected String LocalDate;

	/**
	 * 14 YYMM*
	 */
	protected String ExpDate;

	/**
	 * 15 MMDD*
	 */
	protected String SettleDate;

	/**
	 * 22*
	 */
	protected String EntryMode;

	/**
	 * 23*
	 */
	protected String PanSeqNo;

	/**
	 * 25
	 */
	protected String SvrCode;

	/**
	 * 26
	 */
	protected String CaptureCode;

	/**
	 * 32*
	 */
	protected String AcquirerID;

	/**
	 * 1磁道数据
	 */
	protected String Track1;

	/**
	 * 35
	 */
	protected String Track2;

	/**
	 * 36
	 */
	protected String Track3;

	/**
	 * 37*
	 */
	protected String RRN;

	/**
	 * 38*
	 */
	protected String AuthCode;

	/**
	 * 39
	 */
	protected String RspCode;

	/**
	 * 41
	 */
	protected String TermID;

	/**
	 * 42
	 */
	protected String MerchID;

	/**
	 * 44 *
	 */
	protected String Field44;

	/**
	 * 48 *
	 */
	protected String Field48;

	/**
	 * 49*
	 */
	protected String CurrencyCode;

	/**
	 * 52
	 */
	protected String PIN;

	/**
	 * 53
	 */
	protected String SecurityInfo;

	/**
	 *  54
	 */
	protected String ExtAmount;

	/**
	 * 55*
	 */
	protected byte[] ICCData;

	/**
	 * 60
	 */
	protected String Field60;

	/**
	 * 61
	 */
	protected String Field61;

	/**
	 * 62
	 */
	protected String Field62;

	/**
	 * 63
	 */
	protected String Field63;

	/**
	 * 交易中文名
	 */
	protected String TransCName;

	/**
	 * 交易英文名 主键 交易初始化设置
	 */
	protected String TransEName;

	/**
	 * 批次号 60_2
	 */
	protected String BatchNo;

	/**
	 * 标记此次交易流水号是否自增
	 */
	protected boolean isTraceNoInc = false;

	/**
	 * 是否允许IC卡降级为磁卡
	 */
	protected boolean isFallBack;

	/**
	 * 使用原交易的第3域和 60.1域
	 */
	protected boolean isUseOrgVal = false;

	protected String F60_1;
	protected String F60_3;

	/**
	 * 22域服务点输入方式
	 */
	public static final int ENTRY_MODE_HAND = 1;
	public static final int ENTRY_MODE_MAG = 2;
	public static final int ENTRY_MODE_ICC = 5;
	public static final int ENTRY_MODE_NFC = 7;
	public static final int ENTRY_MODE_QRC = 9;

	/***
	 * Trans 构造
	 * @param ctx
	 * @param transEname
	 */
	public Trans(Context ctx, String transEname) {
		this.context = ctx;
		this.TransEName = transEname;
		this.timeout = 60 * 1000 ;
		loadConfig();
		transLog = TransLog.getInstance();
	}

	/**
	 * 加载初始设置
	 */
	private void loadConfig() {
		Logger.debug("==Trans->loadConfig==");
		cfg = TMConfig.getInstance();
		TermID = cfg.getTermID();
		MerchID = cfg.getMerchID();
		CurrencyCode = cfg.getCurrencyCode();
		BatchNo = ISOUtil.padleft("" + cfg.getBatchNo(), 6, '0');
		TraceNo = ISOUtil.padleft("" + cfg.getTraceNo(), 6, '0');
		boolean isPub = cfg.getPubCommun() ;
		String ip = isPub?cfg.getIp():cfg.getIP2();

		int port = Integer.parseInt(isPub?cfg.getPort():cfg.getPort2());
		int timeout = cfg.getTimeout();
		String tpdu = cfg.getTpdu();
		String header = cfg.getHeader();
		setFixedDatas();
		netWork = new NetworkHelper(ip, port, timeout, context);
		iso8583 = new ISO8583(this.context, tpdu, header);
	}

	/**
	 * 设置消息类型及60域3个子域数据
	 */
	protected void setFixedDatas() {
		Logger.debug("==Trans->setFixedDatas==");
		if (null == TransEName) {
			return;
		}
		Properties pro = PAYUtils.lodeConfig(context, TMConstants.TRANS);
		if (pro == null) {
			return;
		}
		String prop = pro.getProperty(TransEName);
		String[] propGroup = prop.split(",");
		if (!PAYUtils.isNullWithTrim(propGroup[0])){
			MsgID = propGroup[0];
		}else{
			MsgID = null;
		}
		if (isUseOrgVal == false) {
			if (!PAYUtils.isNullWithTrim(propGroup[1])){
				ProcCode = propGroup[1];
			}else{
				ProcCode = null;
			}
		}
		if (!PAYUtils.isNullWithTrim(propGroup[2])){
			SvrCode = propGroup[2];
		}else{
			SvrCode = null;
		}
		if (isUseOrgVal == false) {
			if (!PAYUtils.isNullWithTrim(propGroup[3])){
				F60_1 = propGroup[3];
			}else{
				F60_1 = null;
			}
		}
		if (!PAYUtils.isNullWithTrim(propGroup[4])) {
			F60_3 = propGroup[4];
		}else {
			F60_3 = null;
		}
		if (F60_1 != null && F60_3 != null){
			Field60 = F60_1 + cfg.getBatchNo() + F60_3;
		}
		try {
			TransCName = new String(propGroup[5].getBytes("ISO-8859-1"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取流水号是否自增
	 * @return
     */
	public boolean isTraceNoInc() {
		return isTraceNoInc;
	}

	/**
	 * 设置流水号是否自增
	 * @param isTraceNoInc
     */
	public void setTraceNoInc(boolean isTraceNoInc) {
		this.isTraceNoInc = isTraceNoInc;
	}

	/**
	 * 追加60域内容
	 * @param f60
     */
	protected void appendField60(String f60) {
		Field60 = Field60 + f60;
	}

	/**
	 * 连接
	 * @return
     */
	protected int connect() {
		return netWork.Connect();
	}

	/**
	 * 发送
	 * @return
     */
	protected int send() {
		byte[] pack = iso8583.packetISO8583();
		if (pack == null) {
			return -1;
		}
		Logger.debug("交易:"+TransEName+"\n发送报文:"+ISOUtil.hexString(pack));
		return netWork.Send(pack);
	}

	/**
	 * 接收
	 * @return
     */
	protected byte[] recive() {
		byte[] recive = null;
		try {
			recive = netWork.Recive(2048, cfg.getTimeout());
		} catch (IOException e) {
			return null;
		}
		if(recive!=null){
			Logger.debug("交易:"+TransEName+"\n接收报文:"+ISOUtil.hexString(recive));
		}
		return recive;
	}

	/**
	 * 联机处理
	 * @return
     */
	protected int OnLineTrans() {
		if (connect() == -1) {
			return Tcode.T_socket_err;
		}
		if (send() == -1) {
			return Tcode.T_send_err;
		}
		byte[] respData = recive();
		netWork.close();
		if (respData == null) {
			return Tcode.T_receive_err;
		}

		int ret = iso8583.unPacketISO8583(respData);
		if (ret == 0) {
			if (isTraceNoInc) {
				cfg.incTraceNo().save();
			}
		}
		return ret;
	}

	/**
	 * 清除关键信息
	 */
	protected void clearPan() {
		Pan = null;
		Track2 = null;
		Track3 = null;
		System.gc();//显示调用清除内存
	}
}

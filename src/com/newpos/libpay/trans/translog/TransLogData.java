package com.newpos.libpay.trans.translog;

import java.io.Serializable;

/**
 * 交易日志详情信息类
 * @author zhouqiang
 */
public class TransLogData implements Serializable {

	/**
	 * 发卡组织 63域 前三位
	 */
	private String IssuerName;
	public String getIssuerName() {
		return IssuerName;
	}

	public void setIssuerName(String issuerName) {
		IssuerName = issuerName;
	}

	/**
	 * 应用密文 此次交易是联机还是脱机
	 */
	private int AAC ;
	public int getAAC() {
		return AAC;
	}

	public void setAAC(int AAC) {
		this.AAC = AAC;
	}

	/**
	 * 标记此次用开方式是否是非接方式
	 */
	private boolean isNFC ;
	public boolean isNFC(){
		return isNFC ;
	}

	public void setNFC(boolean isNFC){
		this.isNFC = isNFC ;
	}

	/**
	 * 标记此次交易用卡方式是否是插卡
	 */
	private boolean isICC;
	public boolean isICC() {
		return isICC;
	}

	public void setICC(boolean isICC) {
		this.isICC = isICC;
	}

	/**
	 * 标记此次交易是否是扫码方式
	 */
	private boolean isScan ;
	public boolean isScan() {
		return isScan;
	}

	public void setScan(boolean scan) {
		isScan = scan;
	}

	private int RecState;/** 初始状态为0，已上送成功1，已上送但是失败2 **/

	/** 小费 **/
	private long TipAmout = 0;
	/** 原交易流水号 **/
	private String BatchNo;

	public String getBatchNo() {
		return BatchNo;
	}

	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}

	/**
	 * 标记如果是消费类交易，此交易是否已经撤销
	 */
	private boolean isVoided;
	public boolean getIsVoided(){
		return isVoided ;
	}

	public void setVoided(boolean isVoided){
		this.isVoided = isVoided ;
	}

	/**
	 * 预授权交易是否已经完成，完成的交易不能再次完成
	 */
	private boolean isPreComp;
	public boolean isPreComp() {
		return isPreComp;
	}

	public void setPreComp(boolean preComp) {
		isPreComp = preComp;
	}

	public int getRecState() {
		return RecState;
	}

	public void setRecState(int recState) {
		RecState = recState;
	}

	/**
	 * 交易英文名称
	 * 详见 @{@link com.newpos.libpay.trans.Trans.Type}
	 */
	private String TransEName;
	public String getEName() {
		return TransEName;
	}

	public void setEName(String eName) {
		TransEName = eName;
	}

	/**
	 * 交易60域
	 */
	private String Field60;
	public String getField60() {
		return Field60;
	}

	public void setField60(String field60) {
		Field60 = field60;
	}

	/**
	 * 第二域卡号，加了*号的字串
	 */
	private String Pan;
	public String getPan() {
		return Pan;
	}

	public void setPan(String pan) {
		Pan = pan;
	}

	/**
	 * 第四域，标记此次交易的金额
	 */
	private Long Amount;
	public Long getAmount() {
		return Amount;
	}

	public void setAmount(Long amount) {
		Amount = amount;
	}

	/**
	 * 如果是卡交易，存储此次交易的卡片55域信息
	 */
	private byte[] ICCData;
	public byte[] getICCData() {
		return ICCData;
	}

	public void setICCData(byte[] iCCData) {
		ICCData = iCCData;
	}

	/**
	 * 第11域 , 交易流水号
	 */
	private String TraceNo;
	public String getTraceNo() {
		return TraceNo;
	}

	public void setTraceNo(String traceNo) {
		TraceNo = traceNo;
	}

	/**
	 * 第12域，交易时间
	 */
	private String LocalTime;
	public String getLocalTime() {
		return LocalTime;
	}

	public void setLocalTime(String localTime) {
		LocalTime = localTime;
	}

	/**
	 * 第13域，交易日期
	 */
	private String LocalDate;
	public String getLocalDate() {
		return LocalDate;
	}

	public void setLocalDate(String localDate) {
		LocalDate = localDate;
	}

	/**
	 * 第14域，卡片有效期
	 */
	private String ExpDate;
	public String getExpDate() {
		return ExpDate;
	}

	public void setExpDate(String expDate) {
		ExpDate = expDate;
	}

	/**
	 * 第15域。交易日期
	 */
	private String SettleDate;
	public String getSettleDate() {
		return SettleDate;
	}

	public void setSettleDate(String settleDate) {
		SettleDate = settleDate;
	}

	/**
	 * 第22域，输入方式
	 */
	private String EntryMode;
	public String getEntryMode() {
		return EntryMode;
	}

	public void setEntryMode(String entryMode) {
		EntryMode = entryMode;
	}

	/**
	 * 第23域，卡序号
	 */
	private String PanSeqNo;
	public String getPanSeqNo() {
		return PanSeqNo;
	}

	/**
	 * 第32域
	 */
	private String AcquirerID;
	public void setPanSeqNo(String panSeqNo) {
		PanSeqNo = panSeqNo;
	}

	public String getAcquirerID() {
		return AcquirerID;
	}

	public void setAcquirerID(String acquirerID) {
		AcquirerID = acquirerID;
	}

	/**
	 * 第37域
	 */
	private String RRN;
	public String getRRN() {
		return RRN;
	}

	public void setRRN(String rRN) {
		RRN = rRN;
	}

	/**
	 * 第38域，认证码
	 */
	private String AuthCode;
	public String getAuthCode() {
		return AuthCode;
	}

	public void setAuthCode(String authCode) {
		AuthCode = authCode;
	}

	/**
	 * 第39域，响应码
	 */
	private String RspCode;
	public String getRspCode() {
		return RspCode;
	}

	public void setRspCode(String rspCode) {
		RspCode = rspCode;
	}

	/**
	 * 第44域，发卡行和收单行
	 */
	private String Field44;
	public String getField44() {
		return Field44;
	}

	public void setField44(String field44) {
		Field44 = field44;
	}

	/**
	 * 第三域，预处理码
	 */
	private String ProcCode;
	public String getProcCode() {
		return ProcCode;
	}

	public void setProcCode(String procCode) {
		ProcCode = procCode;
	}

	/**
	 * 第49域，交易货币代码
	 */
	private String CurrencyCode;
	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	/**
	 * 备注 63 从第三位开始到最后
	 */
	private String Refence;
	public String getRefence() {
		return Refence;
	}

	public void setRefence(String refence) {
		Refence = refence;
	}

	/**
	 * 操作员号码
	 */
	private int oprNo;
	public int getOprNo() {
		return oprNo;
	}

	public void setOprNo(int oprNo) {
		this.oprNo = oprNo;
	}

	/**
	 * 第25域 ， 服务点条件吗
	 */
	private String SvrCode;
	public String getSvrCode() {
		return SvrCode;
	}

	public void setSvrCode(String svrCode) {
		SvrCode = svrCode;
	}
}
